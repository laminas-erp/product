<?php

namespace Lerp\Product\Factory\Service\Workflow;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Product\Service\Workflow\WorkflowService;
use Lerp\Product\Table\Workflow\WorkflowTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class WorkflowServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new WorkflowService();
		$service->setLogger($container->get('logger'));
        $service->setWorkflowTable($container->get(WorkflowTable::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $service->setWorkflowTypes($toolsTable->getEnumValuesPostgreSQL('enum_workflow_type'));
		return $service;
	}
}
