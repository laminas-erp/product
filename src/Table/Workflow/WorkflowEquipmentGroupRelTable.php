<?php

namespace Lerp\Product\Table\Workflow;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class WorkflowEquipmentGroupRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'workflow_equipment_group_rel';

    /**
     * @param string $equipGroupUuid
     * @param string $workflowUuid
     * @return bool
     */
    public function existWorkflowEquipmentGroupRel(string $equipGroupUuid, string $workflowUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'equipment_group_uuid' => $equipGroupUuid,
                'workflow_uuid'        => $workflowUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $equipGroupUuid
     * @param string $workflowUuid
     * @return string workflow_equipment_group_rel_uuid
     */
    public function insertWorkflowEquipmentGroupRel(string $equipGroupUuid, string $workflowUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'workflow_equipment_group_rel_uuid' => $uuid,
                'equipment_group_uuid'              => $equipGroupUuid,
                'workflow_uuid'                     => $workflowUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $equipGroupUuid
     * @param string $workflowUuid
     * @return bool
     */
    public function deleteWorkflowEquipmentGroupRel(string $equipGroupUuid, string $workflowUuid): bool
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'equipment_group_uuid' => $equipGroupUuid,
                'workflow_uuid'        => $workflowUuid,
            ]);
            return $this->deleteWith($delete) >= 1;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $workflowUuid
     * @param string $workflowUuidReplace
     * @return int
     */
    public function updateWorkflowUuid(string $workflowUuid, string $workflowUuidReplace): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['workflow_uuid' => $workflowUuidReplace]);
            $update->where(['workflow_uuid' => $workflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
