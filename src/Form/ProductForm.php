<?php

namespace Lerp\Product\Form;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\Boolean;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ProductForm extends AbstractForm implements InputFilterProviderInterface
{
    protected bool $productUuidAvailable = true;
    protected array $structures = [];
    protected array $origins = [];
    protected array $types = [];
    protected array $productCclassIdAssoc = [];
    protected array $productGroupIdAssoc = [];
    protected Adapter $adapterDefault;

    /**
     *
     */
    public function init()
    {
        if ($this->productUuidAvailable) {
            $this->add(['name' => 'product_uuid']);
        }

        $this->add(['name' => 'product_structure']);
        $this->add(['name' => 'product_origin']);
        $this->add(['name' => 'product_type']);

        $this->add(['name' => 'product_version_extern']);
        $this->add(['name' => 'product_revision_extern']);

        $this->add(['name' => 'product_uuid_origin']);

        $this->add(['name' => 'product_cclass_id']);
        $this->add(['name' => 'product_group_id']);
        $this->add(['name' => 'product_dirty']);
        $this->add(['name' => 'quantityunit_uuid']);

        $this->add(['name' => 'product_no_extern']);
        $this->add(['name' => 'product_spec_extern']);
        $this->add(['name' => 'product_text_part']);
        $this->add(['name' => 'product_text_short']);
        $this->add(['name' => 'product_text_long']);
        $this->add(['name' => 'product_text_license']);
        $this->add(['name' => 'product_text_license_remark']);

        $this->add(['name' => 'product_v_version']);
        $this->add(['name' => 'product_v_revision']);

        $this->add(['name' => 'product_briefing']);
        $this->add(['name' => 'product_taxp']);

    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->productUuidAvailable) {
            $filter['product_uuid'] = [
                'required'   => true,
                'filters'    => [['name' => StringTrim::class]],
                'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['product_structure'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class]
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => $this->structures
                    ]
                ]
            ]
        ];

        $filter['product_origin'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class]
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => $this->origins
                    ]
                ]
            ]
        ];

        $filter['product_type'] = [
            'required'      => true,
            'filters'       => [
                ['name' => StringTrim::class]
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => $this->types
                    ]
                ]
            ]
        ];

        $filter['product_version_extern'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_revision_extern'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_uuid_origin'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => Uuid::class
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'table'   => 'product',
                        'field'   => 'product_uuid',
                        'adapter' => $this->adapterDefault
                    ]
                ]
            ]
        ];

        $filter['product_cclass_id'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->productCclassIdAssoc)
                    ]
                ]
            ]
        ];

        $filter['product_group_id'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->productGroupIdAssoc)
                    ]
                ]
            ]
        ];

        $filter['product_dirty'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['quantityunit_uuid'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class]
            ],
            'validators' => [
                [
                    'name' => Uuid::class
                ],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'table'   => 'quantityunit',
                        'field'   => 'quantityunit_uuid',
                        'adapter' => $this->adapterDefault
                    ]
                ]
            ]
        ];

        $filter['product_no_extern'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_spec_extern'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_text_part'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 280,
                    ]
                ]
            ]
        ];

        $filter['product_text_short'] = [
            'required'   => true,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_text_long'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
//                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 64000,
                    ]
                ]
            ]
        ];

        $filter['product_text_license'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        $filter['product_text_license_remark'] = [
            'required'   => false,
            'filters'    => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                //['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 64000,
                    ]
                ]
            ]
        ];

        $filter['product_v_version'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_v_revision'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 140,
                    ]
                ]
            ]
        ];

        $filter['product_briefing'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        $filter['product_taxp'] = [
            'required'      => false,
            'filters'       => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Digits::class,
                ]
            ]
        ];

        return $filter;
    }

    /**
     * @param bool $productUuidAvailable
     */
    public function setProductUuidAvailable(bool $productUuidAvailable): void
    {
        $this->productUuidAvailable = $productUuidAvailable;
    }

    /**
     * @param array $structures
     */
    public function setStructures(array $structures): void
    {
        $this->structures = $structures;
    }

    /**
     * @param array $origins
     */
    public function setOrigins(array $origins): void
    {
        $this->origins = $origins;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    /**
     * @param array $productCclassIdAssoc
     */
    public function setProductCclassIdAssoc(array $productCclassIdAssoc): void
    {
        $this->productCclassIdAssoc = $productCclassIdAssoc;
    }

    /**
     * @param array $productGroupIdAssoc
     */
    public function setProductGroupIdAssoc(array $productGroupIdAssoc): void
    {
        $this->productGroupIdAssoc = $productGroupIdAssoc;
    }

    /**
     * @param Adapter $adapterDefault
     */
    public function setAdapterDefault(Adapter $adapterDefault): void
    {
        $this->adapterDefault = $adapterDefault;
    }
}
