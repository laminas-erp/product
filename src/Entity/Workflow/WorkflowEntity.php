<?php

namespace Lerp\Product\Entity\Workflow;

use Bitkorn\Trinket\Entity\AbstractEntity;

class WorkflowEntity extends AbstractEntity
{
    public array $mapping = [
        'workflow_uuid'           => 'workflow_uuid',
        'workflow_type'           => 'workflow_type',
        'workflow_label'          => 'workflow_label',
        'workflow_text'           => 'workflow_text',
        'workflow_price_per_hour' => 'workflow_price_per_hour',
        'workflow_code'           => 'workflow_code',
        'workflow_is_extern'      => 'workflow_is_extern',
    ];

    protected $primaryKey = 'workflow_uuid';

    public function getWorkflowUuid(): string
    {
        if (!isset($this->storage['workflow_uuid'])) {
            return '';
        }
        return $this->storage['workflow_uuid'];
    }

    public function setWorkflowUuid(string $workflowUuid): void
    {
        $this->storage['workflow_uuid'] = $workflowUuid;
    }

    public function getWorkflowType(): string
    {
        if (!isset($this->storage['workflow_type'])) {
            return '';
        }
        return $this->storage['workflow_type'];
    }

    public function setWorkflowType(string $workflowType): void
    {
        $this->storage['workflow_type'] = $workflowType;
    }

    public function getWorkflowLabel(): string
    {
        if (!isset($this->storage['workflow_label'])) {
            return '';
        }
        return $this->storage['workflow_label'];
    }

    public function setWorkflowLabel(string $workflowLabel): void
    {
        $this->storage['workflow_label'] = $workflowLabel;
    }

    public function getWorkflowText(): string
    {
        if (!isset($this->storage['workflow_text'])) {
            return '';
        }
        return $this->storage['workflow_text'];
    }

    public function setWorkflowText(string $workflowText): void
    {
        $this->storage['workflow_text'] = $workflowText;
    }

    public function getWorkflowPricePerHour(): float
    {
        if (!isset($this->storage['workflow_price_per_hour'])) {
            return 0;
        }
        return $this->storage['workflow_price_per_hour'];
    }

    public function setWorkflowPricePerHour(float $workflowPricePerHour): void
    {
        $this->storage['workflow_price_per_hour'] = $workflowPricePerHour;
    }

    public function getWorkflowCode(): string
    {
        if (!isset($this->storage['workflow_code'])) {
            return '';
        }
        return $this->storage['workflow_code'];
    }

    public function setWorkflowCode(string $workflowCode): void
    {
        $this->storage['workflow_code'] = $workflowCode;
    }

    public function getWorkflowIsExtern(): bool
    {
        if (!isset($this->storage['workflow_is_extern'])) {
            return false;
        }
        return $this->storage['workflow_is_extern'] == true;
    }

    public function setWorkflowIsExtern(bool $workflowIsExtern): void
    {
        $this->storage['workflow_is_extern'] = $workflowIsExtern;
    }
}
