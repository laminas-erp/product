<?php

namespace Lerp\Product\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Log\Logger;

class EnumsService extends AbstractService
{
    protected Adapter $adapterDefault;

    public function setAdapterDefault(Adapter $adapterDefault): void
    {
        $this->adapterDefault = $adapterDefault;
    }

    /**
     * @return array
     */
    public function getEnumListProductStructure(): array
    {
        $stmt = $this->adapterDefault->createStatement('SELECT unnest(enum_range(NULL::enum_product_structure)) AS structure');
        $result = $stmt->execute();
        if (!$result->valid()) {
            return [];
        }
        $enumList = [];
        do {
            $current = $result->current()['structure'];
            $enumList[$current] = $current;
            $result->next();
        } while ($result->valid());
        return $enumList;
    }

    /**
     * @return array
     */
    public function getEnumListProductOrigin(): array
    {
        $stmt = $this->adapterDefault->createStatement('SELECT unnest(enum_range(NULL::enum_product_origin)) AS origin');
        $result = $stmt->execute();
        if (!$result->valid()) {
            return [];
        }
        $enumList = [];
        do {
            $current = $result->current()['origin'];
            $enumList[$current] = $current;
            $result->next();
        } while ($result->valid());
        return $enumList;
    }

    /**
     * @return array
     */
    public function getEnumListProductType(): array
    {
        $stmt = $this->adapterDefault->createStatement('SELECT unnest(enum_range(NULL::enum_product_type)) AS type');
        $result = $stmt->execute();
        if (!$result->valid()) {
            return [];
        }
        $enumList = [];
        do {
            $current = $result->current()['type'];
            $enumList[$current] = $current;
            $result->next();
        } while ($result->valid());
        return $enumList;
    }
}
