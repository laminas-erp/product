<?php

namespace Lerp\Product\Service\Calc;

use Lerp\ProductCalc\Entity\ProductCalcEntity;

interface ProductCalcServiceInterface
{
    /**
     * Create and return an entry for table `product_calc`.
     * @param string $productUuid `product_uuid` for `product_calc.product_uuid`.
     * @param string $userUuid `user_uuid` for `product_calc.user_uuid_create`.
     * @return array
     */
    public function createGetProductCalc(string $productUuid, string $userUuid): array;

    /**
     * Calculate an available product_calc (with `product_calc_uuid`).
     * @param string $productCalcUuid The `product_calc_uuid`.
     * @return ProductCalcEntity|null
     */
    public function calculateProductCalc(string $productCalcUuid): ProductCalcEntity|null;

    /**
     * Get a productCalc from table `product_calc`.
     * @param string $productCalcUuid
     * @return array The productCalc.
     */
    public function getProductCalc(string $productCalcUuid): array;

    /**
     * Update a productCalc in table `product_calc`.
     * @param array $productCalc
     * @param string $userUuid `user_uuid` for `product_calc.user_uuid_update`.
     * @return bool
     */
    public function updateProductCalc(array $productCalc, string $userUuid): bool;

    /**
     * @param ProductCalcEntity $productCalcEntity
     * @param string $userUuid `user_uuid` for `product_calc.user_uuid_update`.
     * @return bool
     */
    public function updateProductCalcWithEntity(ProductCalcEntity $productCalcEntity, string $userUuid): bool;
}
