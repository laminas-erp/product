<?php

namespace Lerp\Product\Factory\Controller\Rest\ProductList;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Product\Controller\Rest\ProductList\ProductListRestController;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Service\ProductService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductListRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ProductListRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductService($container->get(ProductService::class));
        $controller->setProductListService($container->get(ProductListService::class));
        return $controller;
    }
}
