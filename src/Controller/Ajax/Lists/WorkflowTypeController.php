<?php

namespace Lerp\Product\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Product\Service\Workflow\WorkflowService;

class WorkflowTypeController extends AbstractUserController
{
    protected WorkflowService $workflowService;

    public function setWorkflowService(WorkflowService $workflowService): void
    {
        $this->workflowService = $workflowService;
    }

    /**
     * @return JsonModel
     */
    public function workflowTypesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->workflowService->getWorkflowTypesAssoc());
        return $jsonModel;
    }
}
