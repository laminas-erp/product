<?php

namespace Lerp\Product\Factory\Service;

use Interop\Container\ContainerInterface;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Product\Table\ProductWorkflowTable;
use Lerp\Product\Table\Workflow\WorkflowTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductWorkflowServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductWorkflowService();
        $service->setLogger($container->get('logger'));
        $service->setProductWorkflowTable($container->get(ProductWorkflowTable::class));
        $service->setWorkflowTable($container->get(WorkflowTable::class));
        return $service;
    }
}
