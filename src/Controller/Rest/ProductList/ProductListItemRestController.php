<?php

namespace Lerp\Product\Controller\Rest\ProductList;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Service\ProductService;
use Laminas\Http\Response;

class ProductListItemRestController extends AbstractUserRestController
{
    protected ProductService $productService;
    protected ProductListService $productListService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setProductListService(ProductListService $productListService): void
    {
        $this->productListService = $productListService;
    }

    /**
     * forbidden if:
     * - product_uuid exist in list up
     * - product_uuid = product_uuid_parent
     * - product_parent.product_structure != 'list'
     *
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!($uv = new Uuid())->isValid($data['product_uuid_parent']) || !$uv->isValid($data['product_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->productListService->validInsertProductList($data['product_uuid_parent'], $data['product_uuid'])) {
            $jsonModel->addMessage($this->productListService->getMessage());
            return $jsonModel;
        }
        if (!empty($productListUuid = $this->productListService->insertProductList(
            $data['product_uuid_parent'],
            $data['product_uuid'],
            floatval($data['product_list_quantity'])))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('product_list_uuid', $productListUuid);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productListService->deleteProductListItem($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data 'field' = field name; 'value' = 'up' | 'down' (if field = 'product_list_order_priority')
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        switch ($data['field']) {
            case 'quantity':
                if ($this->productListService->updateListItemQuantity($id, floatval($data['value']))) {
                    $jsonModel->setSuccess(1);
                }
                break;
            case 'order_priority':
                if ($this->productListService->updateListItemOrderPriority($id, filter_var($data['value'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
                    $jsonModel->setSuccess(1);
                }
                break;
        }

        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
