<?php

namespace Lerp\Product\Controller\Rest\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Lerp\Product\Entity\Maint\ProductMaintDraftEntity;
use Lerp\Product\Form\Maint\ProductMaintDraftForm;
use Lerp\Product\Service\Maint\ProductMaintService;

class ProductMaintDraftController extends AbstractUserRestController
{
    protected ProductMaintDraftForm $productMaintDraftForm;
    protected ProductMaintService $productMaintService;

    public function setProductMaintDraftForm(ProductMaintDraftForm $productMaintDraftForm): void
    {
        $this->productMaintDraftForm = $productMaintDraftForm;
    }

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productMaintDraftForm->setData($data);
        if (!$this->productMaintDraftForm->isValid()) {
            $jsonModel->setVariables($this->productMaintDraftForm->getMessages());
            return $jsonModel;
        }
        $entity = new ProductMaintDraftEntity();
        if (
            !$entity->exchangeArrayFromDatabase($this->productMaintDraftForm->getData())
            || empty($uuid = $this->productMaintService->insertProductMaintDraft($entity->getStorage()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET all ProductMaintDrafts.
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $asAssoc = filter_var($this->params()->fromQuery('as_assoc', 0), FILTER_SANITIZE_NUMBER_INT);
        if (empty($asAssoc)) {
            $productMaintDrafts = $this->productMaintService->getProductMaintDrafts();
        } else {
            $productMaintDrafts = $this->productMaintService->getProductMaintDraftsUuidAssoc();
        }
        if (!empty($productMaintDrafts)) {
            $jsonModel->setArr($productMaintDrafts);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
