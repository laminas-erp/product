<?php

namespace Lerp\Product\Controller\Ajax\Workflow;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Product\Service\ProductService;

class ProductWorkflowAjaxController extends AbstractUserController
{
    protected ProductService $productService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    /**
     * @return JsonModel
     */
    public function updateProductBriefingAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($uuid = $this->params('product_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $briefing = filter_input(INPUT_POST, 'product_briefing', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ($this->productService->updateProductBriefing($uuid, $briefing)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
