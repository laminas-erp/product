<?php

namespace Lerp\Product\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Product\Form\ProductWorkflowForm;
use Lerp\Product\Service\ProductWorkflowService;
use Laminas\Http\Response;

class ProductWorkflowRestController extends AbstractUserRestController
{
    protected ProductWorkflowService $productWorkflowService;
    protected ProductWorkflowForm $workflowForm;

    public function setProductWorkflowService(ProductWorkflowService $productWorkflowService): void
    {
        $this->productWorkflowService = $productWorkflowService;
    }

    public function setWorkflowForm(ProductWorkflowForm $workflowForm): void
    {
        $this->workflowForm = $workflowForm;
    }

    /**
     * GET a list of workflows for one product.
     * query param `product_uuid`
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid(($productUuid = $this->params()->fromQuery('product_uuid')))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->productWorkflowService->getProductWorkflows($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->workflowForm->setIsNew(true);
        $this->workflowForm->setData($data);
        if (!$this->workflowForm->isValid()) {
            $jsonModel->addMessages($this->workflowForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->workflowForm->getData();
        if (!empty($workflowUuid = $this->productWorkflowService->insertProductWorkflow(
            $formData['product_uuid'],
            $formData['workflow_uuid'],
            $formData['product_workflow_price_per_hour']
        )
        )) {
            $jsonModel->setVariable('workflow_uuid', $workflowUuid);
            $jsonModel->setSuccess(1);
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        }

        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id product_workflow_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productWorkflowService->deleteWorkflow($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id product_workflow_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($data['move']) && !empty($data['direction'])) {
            if ($this->productWorkflowService->updateWorkflowOrderPriority($id, $data['direction'])) {
                $jsonModel->setSuccess(1);
            }
        } else {
            $data['product_workflow_uuid'] = $id;
            $this->workflowForm->setData($data);
            if (!$this->workflowForm->isValid()) {
                $jsonModel->addMessages($this->workflowForm->getMessages());
                return $jsonModel;
            }
            $formData = $this->workflowForm->getData();
            if ($this->productWorkflowService->updateProductWorkflow(
                $data['product_workflow_uuid'],
                $formData['workflow_uuid'],
                $formData['product_workflow_time'],
                $formData['product_workflow_price_per_hour'],
                $formData['product_workflow_order_priority'],
                $formData['product_workflow_text']
            )
            ) {
                $jsonModel->setSuccess(1);
            }
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
