<?php

namespace Lerp\Product\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class ProductListJoinEntity
 * @package Lerp\Product\Entity
 *
 * Mapping/storage fields comes from \Lerp\Product\Table\ProductListTable()->getProductListAll(string $productUuid): array
 * @todo mit Gettern waere es nuetzlicher
 */
class ProductListJoinEntity extends AbstractEntity
{

    protected array $mapping = [
        'product_list_uuid' => 'product_list_uuid',
        'product_uuid_parent' => 'product_uuid_parent',
        'product_uuid' => 'product_uuid',
        'product_list_quantity' => 'product_list_quantity',
        'product_list_order_priority' => 'product_list_order_priority',
        'product_calc_uuid' => 'product_calc_uuid',
        'product_calc_time_create' => 'product_calc_time_create',
        'product_calc_time_update' => 'product_calc_time_update',
        'user_uuid_create' => 'user_uuid_create',
        'user_uuid_update' => 'user_uuid_update',
        'product_calc_cost_base' => 'product_calc_cost_base',
        'product_calc_cost_work' => 'product_calc_cost_work',
        'product_calc_cost_work_extern' => 'product_calc_cost_work_extern',
        'product_calc_cost_base_res' => 'product_calc_cost_base_res',
        'product_calc_cost_work_res' => 'product_calc_cost_work_res',
        'product_calc_manufact_price' => 'product_calc_manufact_price',
        'product_calc_price' => 'product_calc_price',
        'product_calc_percent_mgk' => 'product_calc_percent_mgk',
        'product_calc_percent_fgk' => 'product_calc_percent_fgk',
        'product_calc_percent_vvgk' => 'product_calc_percent_vvgk',
        'product_calc_percent_profit' => 'product_calc_percent_profit',
        'product_calc_percent_profit_extern' => 'product_calc_percent_profit_extern',
        'product_calc_sum_cost_base' => 'product_calc_sum_cost_base',
        'product_calc_sum_cost_work' => 'product_calc_sum_cost_work',
        'product_calc_sum_cost_work_extern' => 'product_calc_sum_cost_work_extern',
        'product_calc_sum_cost_base_res' => 'product_calc_sum_cost_base_res',
        'product_calc_sum_cost_work_res' => 'product_calc_sum_cost_work_res',
        'product_calc_sum_manufact_price' => 'product_calc_sum_manufact_price',
        'product_calc_sum_price' => 'product_calc_sum_price',
        'product_calc_time' => 'product_calc_time',
        'product_calc_price_set' => 'product_calc_price_set',
        'product_structure' => 'product_structure',
        'product_origin' => 'product_origin',
        'product_type' => 'product_type',
        'product_cclass_id' => 'product_cclass_id',
        'product_group_id' => 'product_group_id',
        'product_text_part' => 'product_text_part',
        'product_text_short' => 'product_text_short',
        'product_text_long' => 'product_text_long',
        'product_no_uuid' => 'product_no_uuid',
        'product_uuid_origin' => 'product_uuid_origin',
        'product_v1' => 'product_v1',
        'product_v2' => 'product_v2',
        'product_v3' => 'product_v3',
        'product_v_version' => 'product_v_version',
        'product_v_revision' => 'product_v_revision',
        'product_version_extern' => 'product_version_extern',
        'product_revision_extern' => 'product_revision_extern',
        'product_no_extern' => 'product_no_extern',
        'product_spec_extern' => 'product_spec_extern',
        'product_dirty' => 'product_dirty',
        'quantityunit_uuid' => 'quantityunit_uuid',
        'shop_product_uuid' => 'shop_product_uuid',
        'product_time_create' => 'product_time_create',
        'product_time_update' => 'product_time_update',
        'product_briefing' => 'product_briefing',
        'product_taxp' => 'product_taxp',
        'product_no_no' => 'product_no_no',
        'quantityunit_name' => 'quantityunit_name',
        'quantityunit_label' => 'quantityunit_label',
        'quantityunit_resolution' => 'quantityunit_resolution',
        'quantityunit_resolution_group' => 'quantityunit_resolution_group',
        'quantityunit_order_priority' => 'quantityunit_order_priority',
    ];
}
