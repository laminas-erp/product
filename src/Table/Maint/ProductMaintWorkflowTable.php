<?php

namespace Lerp\Product\Table\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Product\Entity\Maint\ProductMaintWorkflowEntity;

class ProductMaintWorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_maint_workflow';

    /**
     * @param string $productMaintWorkflowUuid
     * @return array
     */
    public function getProductMaintWorkflow(string $productMaintWorkflowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_maint_workflow_uuid' => $productMaintWorkflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * JOIN product_maint
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaintWorkflowsForProductMaint(string $productMaintUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('product_maint', 'product_maint.product_maint_uuid = product_maint_workflow.product_maint_uuid');
            $select->where(['product_maint_workflow.product_maint_uuid' => $productMaintUuid]);
            $select->order('product_maint_workflow_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaintWorkflowsForProductMaintUuidAssoc(string $productMaintUuid): array
    {
        $uuidAssoc = [];
        $result = $this->getProductMaintWorkflowsForProductMaint($productMaintUuid);
        if (is_array($result) && !empty($result)) {
            foreach ($result as $row) {
                $uuidAssoc[$row['product_maint_workflow_uuid']] = $row['product_maint_workflow_label'];
            }
        }
        return $uuidAssoc;
    }

    public function getProductMaintWorkflowOrderPriorityMax(string $productMaintUuid, bool $doMax = true): int
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_maint_uuid' => $productMaintUuid]);
            $select->order('product_maint_workflow_order_priority ' . ($doMax ? 'DESC' : 'ASC'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0]['product_maint_workflow_order_priority'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param array $storage
     * @return string
     */
    public function insertProductMaintWorkflow(array $storage): string
    {
        $insert = $this->sql->insert();
        if (empty($storage)) {
            return '';
        }
        $uuid = $this->uuid();
        $storage['product_maint_workflow_uuid'] = $uuid;
        $storage['product_maint_workflow_order_priority'] = $this->getProductMaintWorkflowOrderPriorityMax($storage['product_maint_uuid']) + 10;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $productUuid
     * @return array All ProductMaintWorkflows for a Product UUID. Join table product_maint and ORDER BY ProductMaint, ProductMaintWorkflow.
     */
    public function getProductMaintWorkflowsForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectProductMaintUuids = new Select('product_maint');
            $selectProductMaintUuids->columns(['product_maint_uuid']);
            $selectProductMaintUuids->where(['product_uuid' => $productUuid]);

            $select->join('product_maint', 'product_maint.product_maint_uuid = product_maint_workflow.product_maint_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where->in('product_maint_workflow.product_maint_uuid', $selectProductMaintUuids);
            $select->order('product_maint_order_priority DESC, product_maint_workflow_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productMaintUuid
     * @param int $orderPriority
     * @return array ['lessThan' => []; 'greaterThan' => []]
     */
    public function getOrderPriorityNeighbors(string $productMaintUuid, int $orderPriority): array
    {
        $neighbors = [];
        $selectLessThan = $this->sql->select();
        $selectGreaterThan = $this->sql->select();
        try {
            $selectLessThan->columns(['product_maint_workflow_uuid', 'product_maint_uuid', 'product_maint_workflow_order_priority']);
            $selectLessThan->where(['product_maint_uuid' => $productMaintUuid]);
            $selectLessThan->where->lessThan('product_maint_workflow_order_priority', $orderPriority);
            $selectLessThan->order('product_maint_workflow_order_priority DESC');
            /** @var HydratingResultSet $resultLessThan */
            $resultLessThan = $this->selectWith($selectLessThan);
            if (!$resultLessThan->valid() || $resultLessThan->count() > 0) {
                $neighbors['lessThan'] = $resultLessThan->toArray()[0];
            }

            $selectGreaterThan->columns(['product_maint_workflow_uuid', 'product_maint_uuid', 'product_maint_workflow_order_priority']);
            $selectLessThan->where(['product_maint_uuid' => $productMaintUuid]);
            $selectGreaterThan->where->greaterThan('product_maint_workflow_order_priority', $orderPriority);
            $selectGreaterThan->order('product_maint_workflow_order_priority ASC');
            /** @var HydratingResultSet $result */
            $resultGreaterThan = $this->selectWith($selectGreaterThan);
            if ($resultGreaterThan->valid() && $resultGreaterThan->count() > 0) {
                $neighbors['greaterThan'] = $resultGreaterThan->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $neighbors;
    }

    /**
     * @param string $productMaintWorkflowUuid
     * @param int $orderPriority
     * @return int
     */
    public function updateOrderPriority(string $productMaintWorkflowUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_maint_workflow_order_priority' => $orderPriority
            ]);
            $update->where(['product_maint_workflow_uuid' => $productMaintWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param ProductMaintWorkflowEntity $entity
     * @return int
     */
    public function updateProductMaintWorkflow(ProductMaintWorkflowEntity $entity): int
    {
        $update = $this->sql->update();
        $storage = $entity->getStorage();
        $uuid = $storage['product_maint_workflow_uuid'];
        unset($storage['product_maint_workflow_uuid']);
        try {
            $update->set($storage);
            $update->where(['product_maint_workflow_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteProductMaintWorkflow(string $productMaintWorkflowUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_maint_workflow_uuid' => $productMaintWorkflowUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteProductMaintWorkflowsForProductMaint(string $productMaintUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_maint_uuid' => $productMaintUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
