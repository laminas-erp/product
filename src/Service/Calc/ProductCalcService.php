<?php

namespace Lerp\Product\Service\Calc;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\ProductCalc\Entity\ProductCalcEntity;

/**
 * Class ProductCalcService
 * @package Lerp\Product\Service\Calc
 *
 * Override it ...e.g. in an own module 'ProductCalc'.
 */
class ProductCalcService extends AbstractService implements ProductCalcServiceInterface
{

    public function createGetProductCalc(string $productUuid, string $userUuid): array
    {
        return [];
    }

    public function calculateProductCalc(string $productCalcUuid): ProductCalcEntity|null
    {
        return null;
    }

    public function getProductCalc(string $productCalcUuid): array
    {
        return [];
    }

    public function updateProductCalc(array $productCalc, string $userUuid): bool
    {
        return false;
    }

    public function updateProductCalcWithEntity(ProductCalcEntity $productCalcEntity, string $userUuid): bool
    {
        return false;
    }
}
