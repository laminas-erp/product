<?php

namespace Lerp\Product\Controller\Rest\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Product\Entity\Maint\ProductMaintEntity;
use Lerp\Product\Form\Maint\ProductMaintForm;
use Lerp\Product\Service\Maint\ProductMaintService;

class ProductMaintController extends AbstractUserRestController
{
    protected ProductMaintForm $productMaintForm;
    protected ProductMaintService $productMaintService;

    public function setProductMaintForm(ProductMaintForm $productMaintForm): void
    {
        $this->productMaintForm = $productMaintForm;
    }

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productMaintForm->setData($data);
        if (!$this->productMaintForm->isValid()) {
            $jsonModel->setVariables($this->productMaintForm->getMessages());
            return $jsonModel;
        }
        $entity = new ProductMaintEntity();
        if (
            !$entity->exchangeArrayFromDatabase($this->productMaintForm->getData())
            || empty($uuid = $this->productMaintService->insertProductMaint($entity->getStorage()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET a ProductMaint with product_maint_uuid.
     *
     * @param string $id The product_maint_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productMaint = $this->productMaintService->getProductMaint($id))) {
            $jsonModel->setObj($productMaint);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET all ProductMaints for a products UUID (GET query parameter).
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params()->fromQuery('product_uuid');
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productMaints = $this->productMaintService->getProductMaintsForProduct($productUuid))) {
            $jsonModel->setArr($productMaints);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productMaintForm->setProductUuidRequired(false);
        $this->productMaintForm->setData($data);
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->productMaintForm->isValid()) {
            $jsonModel->setVariables($this->productMaintForm->getMessages());
            return $jsonModel;
        }
        $entity = new ProductMaintEntity();
        $formData = ArrayUtils::merge($this->productMaintForm->getData(), ['product_maint_uuid' => $id]);
        if (!$entity->exchangeArrayFromDatabase($formData)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $entity->unsetEmptyValues();
        if ($this->productMaintService->updateProductMaint($entity)) {
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productMaintService->deleteProductMaint($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
