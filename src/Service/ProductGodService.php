<?php

namespace Lerp\Product\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\Maint\ProductMaintDraftTable;
use Lerp\Product\Table\Maint\ProductMaintTable;
use Lerp\Product\Table\Maint\ProductMaintWorkflowTable;
use Lerp\Product\Table\ProductCalcTable;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductLocationTable;
use Lerp\Product\Table\ProductNoTable;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Product\Table\ProductWorkflowTable;

class ProductGodService extends AbstractService
{
    protected ProductTable $productTable;
    protected ProductCalcTable $productCalcTable;
    protected ProductListTable $productListTable;
    protected ProductLocationTable $productLocationTable;
    protected ProductMaintTable $productMaintTable;
    protected ProductMaintDraftTable $productMaintDraftTable;
    protected ProductMaintWorkflowTable $productMaintWorkflowTable;
    protected ProductNoTable $productNoTable;
    protected ProductTextTable $productTextTable;
    protected ProductWorkflowTable $productWorkflowTable;

    /**
     * Delete all (in all product tables) for a product.
     *
     * @param string $productUuid
     * @return bool
     */
    public function deleteProductDeep(string $productUuid): bool
    {

    }
}
