<?php

namespace Lerp\Product\Factory\Controller\Ajax\Lists;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Product\Controller\Ajax\Lists\ProductTablesController;
use Lerp\Product\Service\Lists\TablesService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductTablesControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ProductTablesController();
		$controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setTablesService($container->get(TablesService::class));
		return $controller;
	}
}
