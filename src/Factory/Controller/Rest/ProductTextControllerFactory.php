<?php

namespace Lerp\Product\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Product\Controller\Rest\ProductTextController;
use Lerp\Product\Form\ProductTextForm;
use Lerp\Product\Service\ProductTextService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductTextControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ProductTextController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductTextForm($container->get(ProductTextForm::class));
        $controller->setProductTextService($container->get(ProductTextService::class));
        return $controller;
    }
}
