<?php

namespace Lerp\Product\Factory\Service;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Product\Service\ProductTextService;
use Lerp\Product\Table\ProductTextTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductTextServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductTextService();
        $service->setLogger($container->get('logger'));
        $service->setProductTextTable($container->get(ProductTextTable::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $service->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        return $service;
    }
}
