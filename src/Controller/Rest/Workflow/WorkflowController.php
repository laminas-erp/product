<?php

namespace Lerp\Product\Controller\Rest\Workflow;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Product\Entity\Workflow\WorkflowEntity;
use Lerp\Product\Form\Workflow\WorkflowForm;
use Lerp\Product\Service\Workflow\WorkflowService;
use Laminas\Http\Response;

class WorkflowController extends AbstractUserRestController
{
    protected WorkflowService $workflowService;
    protected WorkflowForm $workflowForm;

    public function setWorkflowService(WorkflowService $workflowService): void
    {
        $this->workflowService = $workflowService;
    }

    public function setWorkflowForm(WorkflowForm $workflowForm): void
    {
        $this->workflowForm = $workflowForm;
    }

    /**
     * GET uuidAssoc array from all workflows
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $assoc = $this->params()->fromQuery('assoc') == 1;
        $jsonModel->setArr($this->workflowService->getWorkflowUuidAssoc($assoc));
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->workflowForm->init();
        $this->workflowForm->setData($data);
        if (!$this->workflowForm->isValid()) {
            $jsonModel->addMessages($this->workflowForm->getMessages());
            return $jsonModel;
        }
        $workflow = new WorkflowEntity();
        if (!$workflow->exchangeArrayFromRequest($this->workflowForm->getData())
            || empty($uuid = $this->workflowService->insertWorkflow($workflow))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if(!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessage('No valid uuid');
            return $jsonModel;
        }
        $data['workflow_uuid'] = $id;
        $this->workflowForm->setPrimaryKeyAvailable(true);
        $this->workflowForm->init();
        $this->workflowForm->setData($data);
        if (!$this->workflowForm->isValid()) {
            $jsonModel->addMessages($this->workflowForm->getMessages());
            return $jsonModel;
        }
        $workflow = new WorkflowEntity();
        if (!$workflow->exchangeArrayFromRequest($this->workflowForm->getData())
            || empty($uuid = $this->workflowService->updateWorkflow($workflow))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
