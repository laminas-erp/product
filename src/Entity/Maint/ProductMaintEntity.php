<?php

namespace Lerp\Product\Entity\Maint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductMaintEntity extends AbstractEntity
{
    public array $mapping = [
        'product_maint_uuid' => 'product_maint_uuid',
        'product_uuid' => 'product_uuid',
        'product_maint_label' => 'product_maint_label',
        'product_maint_desc' => 'product_maint_desc',
        'product_maint_order_priority' => 'product_maint_order_priority',
    ];
}
