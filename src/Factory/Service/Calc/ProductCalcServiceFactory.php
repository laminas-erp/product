<?php

namespace Lerp\Product\Factory\Service\Calc;

use Interop\Container\ContainerInterface;
use Lerp\Product\Service\Calc\ProductCalcService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductCalcServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductCalcService();
        $service->setLogger($container->get('logger'));
        return $service;
    }
}
