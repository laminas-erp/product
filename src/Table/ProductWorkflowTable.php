<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductWorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_workflow';

    /**
     * @param string $productWorkflowUuid
     * @return array
     */
    public function getProductWorkflow(string $productWorkflowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_workflow_uuid' => $productWorkflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @param string $workflowType One entry from db.enum_workflow_type('craft', 'service', 'misc')
     * @param bool|null $extern If isset then use it for WHERE workflow_is_extern
     * @return array
     */
    public function getProductWorkflows(string $productUuid, string $workflowType = '', bool $extern = null): array
    {
        $select = $this->sql->select();
        try {
            $select->join('workflow', 'workflow.workflow_uuid = product_workflow.workflow_uuid'
                , ['workflow_type', 'workflow_label', 'workflow_text', 'workflow_price_per_hour', 'workflow_code', 'workflow_is_extern']
                , Select::JOIN_LEFT);
            $select->where(['product_uuid' => $productUuid]);
            if (!empty($workflowType)) {
                $select->where(['workflow_type' => $workflowType]);
            }
            if (isset($extern) && is_bool($extern)) {
                $select->where(['workflow_is_extern' => $extern]);
            }
            $select->order('product_workflow_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getProductWorkflowsByProductWorkflowUuid(string $productWorkflowUuid): array
    {
        $workflow = $this->getProductWorkflow($productWorkflowUuid);
        if (empty($workflow)) {
            return [];
        }
        return $this->getProductWorkflows($workflow['product_uuid']);
    }

    /**
     * @param string $productUuid
     * @param string $workflowUuid
     * @param float $pricePerHour
     * @param string $workflowText
     * @param int $workflowTime
     * @return string
     */
    public function insertProductWorkflow(string $productUuid, string $workflowUuid, float $pricePerHour, string $workflowText, int $workflowTime = 0): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'product_workflow_uuid'           => $uuid,
                'product_uuid'                    => $productUuid,
                'workflow_uuid'                   => $workflowUuid,
                'product_workflow_time'           => $workflowTime,
                'product_workflow_price_per_hour' => $pricePerHour,
                'product_workflow_order_priority' => $this->getProductWorkflowOrderPriorityNext($productUuid),
                'product_workflow_text'           => $workflowText,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getProductWorkflowOrderPriorityNext(string $productUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['min_priority' => new Expression('MIN(product_workflow_order_priority)')]);
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0 && !empty($result->current()['min_priority'])) {
                return intval($result->current()['min_priority']) - 10;
            } else {
                return 10000;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productWorkflowUuid
     * @param string $workflowUuid
     * @param int $workflowTime
     * @param float $pricePerHour
     * @param int $orderPriority
     * @param string $workflowText
     * @return int
     */
    public function updateProductWorkflow(string $productWorkflowUuid, string $workflowUuid, int $workflowTime, float $pricePerHour, int $orderPriority, string $workflowText): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'workflow_uuid'                   => $workflowUuid,
                'product_workflow_time'           => $workflowTime,
                'product_workflow_price_per_hour' => $pricePerHour,
                'product_workflow_order_priority' => $orderPriority,
                'product_workflow_time_update'    => new Expression('CURRENT_TIMESTAMP'),
                'product_workflow_text'           => $workflowText,
            ]);
            $update->where(['product_workflow_uuid' => $productWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $workflowUuid
     * @return int
     */
    public function deleteProductWorkflow(string $workflowUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_workflow_uuid' => $workflowUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateProductWorkflowOrderPriority(string $productWorkflowUuid, string $direc): bool
    {
        $neighbors = $this->getProductWorkflowsByProductWorkflowUuid($productWorkflowUuid);
        if (empty($neighbors)) {
            false;
        }
        foreach ($neighbors as $i => $neighbor) {
            if ($neighbor['product_workflow_uuid'] == $productWorkflowUuid) {
                $self = $neighbor;
                $top = isset($neighbors[$i - 1]) ? $neighbors[$i - 1] : null;
                $down = isset($neighbors[$i + 1]) ? $neighbors[$i + 1] : null;
            }
        }
        if ($direc === 'up') {
            if (!isset($top)) {
                return false;
            }
            $this->updateProductWorkflowOrderPriorityConcrete($top['product_workflow_uuid'], $self['product_workflow_order_priority']);
            $this->updateProductWorkflowOrderPriorityConcrete($self['product_workflow_uuid'], $top['product_workflow_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($down)) {
                return false;
            }
            $this->updateProductWorkflowOrderPriorityConcrete($down['product_workflow_uuid'], $self['product_workflow_order_priority']);
            $this->updateProductWorkflowOrderPriorityConcrete($self['product_workflow_uuid'], $down['product_workflow_order_priority']);
        }
        return true;
    }

    protected function updateProductWorkflowOrderPriorityConcrete(string $productWorkflowUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_workflow_order_priority' => $orderPriority,
                'product_workflow_time_update'    => new Expression('CURRENT_TIMESTAMP')
            ]);
            $update->where(['product_workflow_uuid' => $productWorkflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $workflowUuid
     * @param string $workflowUuidReplace
     * @return int
     */
    public function updateWorkflowUuid(string $workflowUuid, string $workflowUuidReplace): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['workflow_uuid' => $workflowUuidReplace]);
            $update->where(['workflow_uuid' => $workflowUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
