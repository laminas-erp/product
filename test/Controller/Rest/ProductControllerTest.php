<?php

namespace Lerp\Product\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ProductControllerTest extends AbstractHttpControllerTestCase
{
    protected function setUp(): void
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
        // Grabbing the full application configuration:
            include __DIR__ . '/../../../../../../config/application.config.php',
            $configOverrides
        ));
        parent::setUp();

        /** @var UserService $userService */
        $userService = $this->getApplication()->getServiceManager()->get(UserService::class);
        $userService->setSessionHashManually('151956ad612953e61533b9d2d5844d65df5b5c8c4e35f5e49375ef77711b6676');
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/lerp-product-product', 'GET', [], true);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('lerp');
        $this->assertControllerName(ProductController::class);
        $this->assertControllerClass('ProductController');
        $this->assertMatchedRouteName('lerp_product_rest_product_product');
    }
}
