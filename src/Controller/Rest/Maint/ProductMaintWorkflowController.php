<?php

namespace Lerp\Product\Controller\Rest\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Product\Entity\Maint\ProductMaintWorkflowEntity;
use Lerp\Product\Form\Maint\ProductMaintWorkflowForm;
use Lerp\Product\Service\Maint\ProductMaintService;

class ProductMaintWorkflowController extends AbstractUserRestController
{
    protected ProductMaintWorkflowForm $productMaintWorkflowForm;
    protected ProductMaintService $productMaintService;

    public function setProductMaintWorkflowForm(ProductMaintWorkflowForm $productMaintWorkflowForm): void
    {
        $this->productMaintWorkflowForm = $productMaintWorkflowForm;
    }

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productMaintWorkflowForm->setData($data);
        if (!$this->productMaintWorkflowForm->isValid()) {
            $jsonModel->addMessages($this->productMaintWorkflowForm->getMessages());
            return $jsonModel;
        }
        $entity = new ProductMaintWorkflowEntity();
        if (
            !$entity->exchangeArrayFromDatabase($this->productMaintWorkflowForm->getData())
            || empty($uuid = $this->productMaintService->insertProductMaintWorkflow($entity->getStorage()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setUuid($uuid);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id product_maint_workflow_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if(!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if(!empty($productMaint = $this->productMaintService->getProductMaintWorkflow($id))) {
            $jsonModel->setObj($productMaint);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET all ProductMaintWorkflows for a productMaint UUID (GET query parameter).
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productMaintUuid = $this->params()->fromQuery('product_maint_uuid');
        if(!(new Uuid())->isValid($productMaintUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $asAssoc = filter_var($this->params()->fromQuery('as_assoc', 0), FILTER_SANITIZE_NUMBER_INT);
        if (empty($asAssoc)) {
            $productMaintWorkflows = $this->productMaintService->getProductMaintWorkflowsForProductMaint($productMaintUuid);
        } else {
            $productMaintWorkflows = $this->productMaintService->getProductMaintWorkflowsForProductMaintUuidAssoc($productMaintUuid);
        }
        if(!empty($productMaintWorkflows)) {
            $jsonModel->setArr($productMaintWorkflows);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productMaintWorkflowForm->setProductMaintUuidRequired(false);
        $this->productMaintWorkflowForm->setData($data);
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->productMaintWorkflowForm->isValid()) {
            $jsonModel->setVariables($this->productMaintWorkflowForm->getMessages());
            return $jsonModel;
        }
        $entity = new ProductMaintWorkflowEntity();
        $formData = ArrayUtils::merge($this->productMaintWorkflowForm->getData(), ['product_maint_workflow_uuid' => $id]);
        if (!$entity->exchangeArrayFromDatabase($formData)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $entity->unsetEmptyValues();
        if($this->productMaintService->updateProductMaintWorkflow($entity)) {
            $jsonModel->setSuccess(1);
        }

        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if($this->productMaintService->deleteProductMaintWorkflow($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
