<?php

namespace Lerp\Product\Service\Maint;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Stdlib\ArrayUtils;
use Lerp\Product\Entity\Maint\ProductMaintEntity;
use Lerp\Product\Entity\Maint\ProductMaintWorkflowEntity;
use Lerp\Product\Table\Maint\ProductMaintDraftTable;
use Lerp\Product\Table\Maint\ProductMaintTable;
use Lerp\Product\Table\Maint\ProductMaintWorkflowTable;

class ProductMaintService extends AbstractService
{
    protected ProductMaintTable $productMaintTable;
    protected ProductMaintDraftTable $productMaintDraftTable;
    protected ProductMaintWorkflowTable $productMaintWorkflowTable;

    public function setProductMaintTable(ProductMaintTable $productMaintTable): void
    {
        $this->productMaintTable = $productMaintTable;
    }

    public function setProductMaintDraftTable(ProductMaintDraftTable $productMaintDraftTable): void
    {
        $this->productMaintDraftTable = $productMaintDraftTable;
    }

    public function setProductMaintWorkflowTable(ProductMaintWorkflowTable $productMaintWorkflowTable): void
    {
        $this->productMaintWorkflowTable = $productMaintWorkflowTable;
    }

    /**
     * Insert a new ProductMaint and return the UUID from the new created ProductMaint.
     *
     * @param array $storage
     * @return string
     */
    public function insertProductMaint(array $storage): string
    {
        return $this->productMaintTable->insertProductMaint($storage);
    }

    /**
     * @param ProductMaintEntity $entity
     * @return bool
     */
    public function updateProductMaint(ProductMaintEntity $entity): bool
    {
        return $this->productMaintTable->updateProductMaint($entity) >= 0;
    }

    public function deleteProductMaint(string $productMaintUuid): bool
    {
        if ($this->productMaintWorkflowTable->deleteProductMaintWorkflowsForProductMaint($productMaintUuid) >= 0) {
            return $this->productMaintTable->deleteProductMaint($productMaintUuid) == 1;
        }
        return false;
    }

    /**
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaint(string $productMaintUuid): array
    {
        return $this->productMaintTable->getProductMaint($productMaintUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductMaintsForProduct(string $productUuid): array
    {
        return $this->productMaintTable->getProductMaintsForProduct($productUuid);
    }

    /**
     * Updates the orderPriority for productMaint and his target productMaint.
     *
     * @param string $productMaintUuid
     * @param string $direc
     * @return bool
     */
    public function updateProductMaintOrderPriority(string $productMaintUuid, string $direc): bool
    {
        if (
            empty($productMaint = $this->productMaintTable->getProductMaint($productMaintUuid))
            || empty($neighbors = $this->productMaintTable->getOrderPriorityNeighbors($productMaint['product_uuid'], $productMaint['product_maint_order_priority']))
        ) {
            false;
        }
        if ($direc === 'up') {
            if (!isset($neighbors['greaterThan'])) {
                return false;
            }
            $this->productMaintTable->updateOrderPriority($productMaint['product_maint_uuid'], $neighbors['greaterThan']['product_maint_order_priority']);
            $this->productMaintTable->updateOrderPriority($neighbors['greaterThan']['product_maint_uuid'], $productMaint['product_maint_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($neighbors['lessThan'])) {
                return false;
            }
            $this->productMaintTable->updateOrderPriority($productMaint['product_maint_uuid'], $neighbors['lessThan']['product_maint_order_priority']);
            $this->productMaintTable->updateOrderPriority($neighbors['lessThan']['product_maint_uuid'], $productMaint['product_maint_order_priority']);
        }
        return true;
    }

    /**
     * Insert a new ProductMaintDraft and return the UUID from the new created ProductMaintDraft.
     *
     * @param array $storage
     * @return string
     */
    public function insertProductMaintDraft(array $storage): string
    {
        return $this->productMaintDraftTable->insertProductMaintDraft($storage);
    }

    /**
     * @return array
     */
    public function getProductMaintDraftsUuidAssoc(): array
    {
        return $this->productMaintDraftTable->getProductMaintDraftsUuidAssoc();
    }

    /**
     * @return array
     */
    public function getProductMaintDrafts(): array
    {
        return $this->productMaintDraftTable->getProductMaintDrafts();
    }

    /**
     * @param array $storage
     * @return string
     */
    public function insertProductMaintWorkflow(array $storage): string
    {
        return $this->productMaintWorkflowTable->insertProductMaintWorkflow($storage);
    }

    /**
     * @param ProductMaintWorkflowEntity $entity
     * @return bool
     */
    public function updateProductMaintWorkflow(ProductMaintWorkflowEntity $entity): bool
    {
        return $this->productMaintWorkflowTable->updateProductMaintWorkflow($entity) >= 0;
    }

    public function deleteProductMaintWorkflow(string $productMaintWorkflowUuid): bool
    {
        return $this->productMaintWorkflowTable->deleteProductMaintWorkflow($productMaintWorkflowUuid) == 1;
    }

    /**
     * @param string $productMaintWorkflowUuid
     * @return array
     */
    public function getProductMaintWorkflow(string $productMaintWorkflowUuid): array
    {
        return $this->productMaintWorkflowTable->getProductMaintWorkflow($productMaintWorkflowUuid);
    }

    /**
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaintWorkflowsForProductMaint(string $productMaintUuid): array
    {
        return $this->productMaintWorkflowTable->getProductMaintWorkflowsForProductMaint($productMaintUuid);
    }

    /**
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaintWorkflowsForProductMaintUuidAssoc(string $productMaintUuid): array
    {
        return $this->productMaintWorkflowTable->getProductMaintWorkflowsForProductMaintUuidAssoc($productMaintUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductMaintWorkflowsForProduct(string $productUuid): array
    {
        return ArrayUtils::merge(
            $this->productMaintWorkflowTable->getProductMaintWorkflowsForProduct($productUuid)
            , $this->productMaintTable->getProductMaintsWithoutWorkflow($productUuid)
        );
    }

    /**
     * @param string $productMaintWorkflowUuid
     * @param string $direc
     * @return bool
     */
    public function updateProductMaintWorkflowOrderPriority(string $productMaintWorkflowUuid, string $direc): bool
    {
        if (
            empty($pmwf = $this->productMaintWorkflowTable->getProductMaintWorkflow($productMaintWorkflowUuid))
            || empty($neighbors = $this->productMaintWorkflowTable->getOrderPriorityNeighbors($pmwf['product_maint_uuid'], $pmwf['product_maint_workflow_order_priority']))
        ) {
            false;
        }
        if ($direc === 'up') {
            if (!isset($neighbors['greaterThan'])) {
                return false;
            }
            $this->productMaintWorkflowTable->updateOrderPriority($pmwf['product_maint_workflow_uuid'], $neighbors['greaterThan']['product_maint_workflow_order_priority']);
            $this->productMaintWorkflowTable->updateOrderPriority($neighbors['greaterThan']['product_maint_workflow_uuid'], $pmwf['product_maint_workflow_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($neighbors['lessThan'])) {
                return false;
            }
            $this->productMaintWorkflowTable->updateOrderPriority($pmwf['product_maint_workflow_uuid'], $neighbors['lessThan']['product_maint_workflow_order_priority']);
            $this->productMaintWorkflowTable->updateOrderPriority($neighbors['lessThan']['product_maint_workflow_uuid'], $pmwf['product_maint_workflow_order_priority']);
        }
        return true;
    }
}
