<?php

namespace Lerp\Product\Controller\Ajax\Lists;

use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Product\Service\Lists\EnumsService;
use Laminas\Http\Response;
use Bitkorn\Trinket\View\Model\JsonModel;

class ProductEnumsController extends AbstractUserController
{
    protected EnumsService $enumsService;

    public function setEnumsService(EnumsService $enumsService): void
    {
        $this->enumsService = $enumsService;
    }

    /**
     * @return JsonModel
     */
    public function productStructureAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->enumsService->getEnumListProductStructure());
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function productOriginAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->enumsService->getEnumListProductOrigin());
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function productTypeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->enumsService->getEnumListProductType());
        return $jsonModel;
    }
}
