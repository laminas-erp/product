<?php

namespace Lerp\Product\Controller\Rest;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Product\Entity\ProductEntity;
use Lerp\Product\Form\ProductForm;
use Lerp\Product\Service\ProductService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Bitkorn\Trinket\View\Model\JsonModel;

class ProductController extends AbstractUserRestController
{
    protected ProductService $productService;
    protected ProductForm $productForm;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setProductForm(ProductForm $productForm): void
    {
        $this->productForm = $productForm;
    }

    /**
     * POST maps to create().
     * The response should typically be an HTTP 201 (created) response
     * with the Location header indicating the URI of the newly created entity
     * and the response body providing the representation.
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->productForm->setProductUuidAvailable(false);
        $this->productForm->init();
        $this->productForm->setData($data);
        if (!$this->productForm->isValid()) {
            $jsonModel->addMessages($this->productForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->productForm->getData();
        $productEntity = new ProductEntity();
        if (!$productEntity->exchangeArrayFromRequest($formData) || empty($productUuid = $this->productService->insertProduct($productEntity))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        $jsonModel->setUuid($productUuid);

        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * Return either a 200 or 202 (Accepted) response status, as well as the representation of the entity.
     * @param string $id Query/URL Parameter
     * @param array $data Data in request body
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        $data['product_uuid'] = $id;
        $this->productForm->init();
        $this->productForm->setData($data);
        if (!$this->productForm->isValid()) {
            $jsonModel->addMessages($this->productForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->productForm->getData();
        $productEntity = new ProductEntity();
        if (!$productEntity->exchangeArrayFromRequest($formData) || !$this->productService->updateProduct($productEntity)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);

        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->productService->getProduct($id));
        return $jsonModel;
    }

    /**
     * GET Search products.
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $filter = new FilterChainStringSanitize();
        $structure = $filter->filter($request->getQuery('product_structure', ''));
        $origin = $filter->filter($request->getQuery('product_origin', ''));
        $type = $filter->filter($request->getQuery('product_type', ''));
        $cclassId = (int)filter_input(INPUT_GET, 'product_cclass_id', FILTER_SANITIZE_NUMBER_INT);
        $groupId = (int)filter_input(INPUT_GET, 'product_group_id', FILTER_SANITIZE_NUMBER_INT);
        $productNo = $filter->filter($request->getQuery('product_no_no', ''));
        $text = $filter->filter($request->getQuery('product_text', ''));
        $dirty = $filter->filter($request->getQuery('product_dirty_select', ''));

        $orderField = $filter->filter($request->getQuery('order_field', ''));
        $orderDirec = $filter->filter($request->getQuery('order_direc', ''));
        $offset = (int)filter_input(INPUT_GET, 'offset', FILTER_SANITIZE_NUMBER_INT);
        $limit = (int)filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);

        if (!empty($productNo)) {
            $orderField = 'product_no_no';
            $orderDirec = 'ASC'; // best match first
        }
        $jsonModel->setArr($this->productService->searchProduct($structure, $origin, $type, $cclassId, $groupId, $productNo, $text, $dirty, $orderField, $orderDirec, $offset, $limit));
        $jsonModel->setCount($this->productService->getSearchProductCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
