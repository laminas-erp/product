<?php

namespace Lerp\Product\Factory\Service;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Common\Service\ConfigService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Product\Service\Lists\EnumsService;
use Lerp\Product\Service\Lists\TablesService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Table\ProductLocationTable;
use Lerp\Product\Table\ProductNoTable;
use Lerp\Product\Table\ProductSupplierTable;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Table\ProductWorkflowTable;

class ProductServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductService();
        $service->setLogger($container->get('logger'));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductNoTable($container->get(ProductNoTable::class));
        $service->setProductTextTable($container->get(ProductTextTable::class));
        $service->setProductLocationTable($container->get(ProductLocationTable::class));
        $service->setProductWorkflowTable($container->get(ProductWorkflowTable::class));
        $service->setProductSupplierTable($container->get(ProductSupplierTable::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        $service->setConfigService($container->get(ConfigService::class));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $service->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        /** @var EnumsService $enumService */
        $enumService = $container->get(EnumsService::class);
        $service->setStructures($enumService->getEnumListProductStructure());
        $service->setOrigins($enumService->getEnumListProductOrigin());
        $service->setTypes($enumService->getEnumListProductType());

        /** @var TablesService $productTableService */
        $productTableService = $container->get(TablesService::class);
        $service->setCclassIds($productTableService->getListProductCclass());
        $service->setGroupIds($productTableService->getListProductGroup());
        return $service;
    }
}
