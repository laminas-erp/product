<?php

namespace Lerp\Product\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Lerp\Product\Service\Lists\TablesService;
use Lerp\Product\Table\Lists\ProductCclassTable;
use Lerp\Product\Table\Lists\ProductGroupTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TablesServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new TablesService();
		$service->setLogger($container->get('logger'));
        $service->setProductCclassTable($container->get(ProductCclassTable::class));
        $service->setProductGroupTable($container->get(ProductGroupTable::class));
		return $service;
	}
}
