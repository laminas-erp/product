<?php

namespace Lerp\Product\Factory\Table;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Lerp\Product\Table\ProductTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductTableFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 * @throws ContainerException if any other error occurs
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$table = new ProductTable();
		$table->setLogger($container->get('logger'));
		$table->setDbAdapter($container->get('dbDefault'));
		return $table;
	}
}
