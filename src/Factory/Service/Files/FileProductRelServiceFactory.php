<?php

namespace Lerp\Product\Factory\Service\Files;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Service\Files\FileProductRelService;
use Lerp\Product\Table\Files\FileProductRelTable;

class FileProductRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileProductRelService();
        $service->setLogger($container->get('logger'));
        $service->setFileProductRelTable($container->get(FileProductRelTable::class));
        $service->setFileService($container->get(FileService::class));
        return $service;
    }
}
