<?php

namespace Lerp\Product\Service\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Product\Table\Files\FileProductRelTable;

class FileProductRelService extends AbstractService
{
    protected FileProductRelTable $fileProductRelTable;
    protected Adapter $adapter;
    protected FileService $fileService;

    public function setFileProductRelTable(FileProductRelTable $fileProductRelTable): void
    {
        $this->fileProductRelTable = $fileProductRelTable;
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $fileProductRelUuid
     * @return array
     */
    public function getFileProductRel(string $fileProductRelUuid): array
    {
        return $this->fileProductRelTable->getFileProductRel($fileProductRelUuid);
    }

    public function getFileProductRelJoined(string $fileProductRelUuid): array
    {
        return $this->fileProductRelTable->getFileProductRelJoined($fileProductRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $productUuid
     * @param string $folderBrand
     * @return string fileProductRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $productUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        return $this->insertFileProductRel($fileUuid, $productUuid);
    }

    /**
     * @param string $filePath
     * @param string $fileLabel
     * @param string $fileDesc
     * @param string $productUuid
     * @param string $folderBrand
     * @return string On success the file_product_rel_uuid ELSE an empty string and $this->message is available.
     */
    public function handleFileCopy(string $filePath, string $fileLabel, string $fileDesc, string $productUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileCopy($filePath, $fileLabel, $fileDesc, $folderBrand))) {
            $this->message = $this->fileService->getMessage();
            return '';
        }
        return $this->insertFileProductRel($fileUuid, $productUuid);
    }

    protected function insertFileProductRel(string $fileUuid, string $productUuid): string
    {
        if (empty($fileProductRelUuid = $this->fileProductRelTable->insertFileProductRel($fileUuid, $productUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileProductRelUuid;
    }

    public function deleteFile(string $fileProductRelUuid): bool
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $fileRel = $this->fileProductRelTable->getFileProductRel($fileProductRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileProductRelTable->deleteFile($fileProductRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForProduct(string $productUuid): array
    {
        return $this->fileProductRelTable->getFilesForProduct($productUuid);
    }

    public function getFilesForProducts(array $productUuids): array
    {
        return $this->fileProductRelTable->getFilesForProducts($productUuids);
    }

    public function updateFile(FileEntity $fileEntity, string $productUuid): bool
    {
        return $this->fileProductRelTable->updateFile($fileEntity, $productUuid) >= 0;
    }
}
