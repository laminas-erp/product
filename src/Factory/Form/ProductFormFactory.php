<?php

namespace Lerp\Product\Factory\Form;

use Interop\Container\ContainerInterface;
use Lerp\Product\Service\Lists\EnumsService;
use Lerp\Product\Form\ProductForm;
use Lerp\Product\Table\Lists\ProductCclassTable;
use Lerp\Product\Table\Lists\ProductGroupTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ProductForm();
        /** @var EnumsService $enumService */
        $enumService = $container->get(EnumsService::class);
        $form->setStructures($enumService->getEnumListProductStructure());
        $form->setOrigins($enumService->getEnumListProductOrigin());
        $form->setTypes($enumService->getEnumListProductType());
        /** @var ProductCclassTable $productCclassTable */
        $productCclassTable = $container->get(ProductCclassTable::class);
        $form->setProductCclassIdAssoc($productCclassTable->getProductCclassIdAssoc());
        /** @var ProductGroupTable $productGroupTable */
        $productGroupTable = $container->get(ProductGroupTable::class);
        $form->setProductGroupIdAssoc($productGroupTable->getProductGroupIdAssoc());
        $form->setAdapterDefault($container->get('dbDefault'));
        return $form;
    }
}
