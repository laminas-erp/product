<?php

namespace Lerp\Product\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Product\Service\ProductService;

class SupplierProductAjaxController extends AbstractUserController
{
    protected ProductService $productService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

	/**
	 * @return JsonModel
	 */
	public function supplierProductsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($supplierUuid = $this->params('supplier_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->productService->getSupplierProducts($supplierUuid));
		return $jsonModel;
	}
}
