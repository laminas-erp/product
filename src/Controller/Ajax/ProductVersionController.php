<?php

namespace Lerp\Product\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Product\Service\ProductService;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class ProductVersionController extends AbstractUserController
{
    protected ProductService $productService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    /**
     * @return JsonModel
     */
    public function productsForNoUuidAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $uuid = $this->params('uuid');
        if (!(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $products = $this->productService->getProductsForNoUuid($uuid);
        $jsonModel->setArr($products);
        $jsonModel->setCount(count($products));
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function copyProductAsSingleAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);

        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function copyProductAsVersionAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);

        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function copyProductAsRevisionAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);

        return $jsonModel;
    }
}
