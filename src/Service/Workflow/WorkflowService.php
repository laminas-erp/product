<?php

namespace Lerp\Product\Service\Workflow;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Entity\Workflow\WorkflowEntity;
use Lerp\Product\Table\Workflow\WorkflowTable;

class WorkflowService extends AbstractService
{
    protected WorkflowTable $workflowTable;
    protected array $workflowTypes;

    public function setWorkflowTable(WorkflowTable $workflowTable): void
    {
        $this->workflowTable = $workflowTable;
    }

    public function setWorkflowTypes(array $workflowTypes): void
    {
        $this->workflowTypes = $workflowTypes;
    }

    /**
     * @param $assoc bool
     * @return array
     */
    public function getWorkflowUuidAssoc(bool $assoc): array
    {
        return $this->workflowTable->getWorkflows($assoc);
    }

    /**
     * @param bool $assoc
     * @return array
     */
    public function getWorkflowTypesAssoc(bool $assoc = false): array
    {
        if ($assoc) {
            $a = [];
            foreach ($this->workflowTypes as $wt) {
                $a[$wt] = $wt;
            }
            return $a;
        }
        return $this->workflowTypes;
    }

    /**
     * @param WorkflowEntity $workflow
     * @return string workflow_uuid
     */
    public function insertWorkflow(WorkflowEntity $workflow): string
    {
        return $this->workflowTable->insertWorkflow($workflow);
    }

    public function updateWorkflow(WorkflowEntity $workflow): bool
    {
        return $this->workflowTable->updateWorkflow($workflow);
    }

    public function getWorkflow(string $workflowUuid): array
    {
        return $this->workflowTable->getWorkflow($workflowUuid);
    }

    public function getWorkflowByLabel(string $workflowLabel, bool $toLower = false): array
    {
        return $this->workflowTable->getWorkflowByLabel($workflowLabel, $toLower);
    }
}
