<?php

namespace Lerp\Product\Form;

use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ProductWorkflowForm extends AbstractForm implements InputFilterProviderInterface
{
    protected bool $isNew = false;

    public function init()
    {
        if (!$this->isNew) {
            $this->add(['name' => 'product_workflow_uuid']);
        } else {
            $this->add(['name' => 'product_uuid']);
        }
        $this->add(['name' => 'workflow_uuid']);
        $this->add(['name' => 'product_workflow_time']);
        $this->add(['name' => 'product_workflow_price_per_hour']);
        $this->add(['name' => 'product_workflow_order_priority']);
        $this->add(['name' => 'product_workflow_text']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if (!$this->isNew) {
            $filter['product_workflow_uuid'] = [
                'required' => true,
                'filters' => [['name' => StringTrim::class]],
                'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        } else {
            $filter['product_uuid'] = [
                'required' => true,
                'filters' => [['name' => StringTrim::class]],
                'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['workflow_uuid'] = [
            'required' => true,
            'filters' => [['name' => StringTrim::class]],
            'validators' => [
                [
                    'name' => Uuid::class
                ]
            ]
        ];

        if (!$this->isNew) {
            $filter['product_workflow_time'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Digits::class
                    ]
                ]
            ];
        }

        $filter['product_workflow_price_per_hour'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => FloatValidator::class
                ]
            ]
        ];

        $filter['product_workflow_order_priority'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Digits::class
                ]
            ]
        ];

        $filter['product_workflow_text'] = [
            'required' => !$this->isNew,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        return $filter;
    }

    /**
     * @param bool $isNew
     */
    public function setIsNew(bool $isNew): void
    {
        $this->isNew = $isNew;
    }
}
