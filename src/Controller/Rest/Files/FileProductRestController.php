<?php

namespace Lerp\Product\Controller\Rest\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Product\Form\Files\FileProductForm;
use Lerp\Product\Service\Files\FileProductRelService;

class FileProductRestController extends AbstractUserRestController
{
    protected string $moduleBrand = '';
    protected FileProductForm $fileProductForm;
    protected FileProductRelService $fileProductRelService;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFileProductForm(FileProductForm $fileProductForm): void
    {
        $this->fileProductForm = $fileProductForm;
    }

    public function setFileProductRelService(FileProductRelService $fileProductRelService): void
    {
        $this->fileProductRelService = $fileProductRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $productUuid = $data['product_uuid'];
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->fileProductForm->getFileFieldset()->setFileInputAvailable(true);
        $this->fileProductForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->fileProductForm->setData($post);
            if ($this->fileProductForm->isValid()) {
                $formData = $this->fileProductForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->fileProductRelService->handleFileUpload($fileEntity, $productUuid, $this->moduleBrand))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->fileProductForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id file_product_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->fileProductRelService->deleteFile($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = filter_input(INPUT_GET, 'product_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($productUuid) && !empty($files = $this->fileProductRelService->getFilesForProduct($productUuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id product_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['product_uuid'] = $id; // currently unused
        $this->fileProductForm->getFileFieldset()->setPrimaryKeyAvailable(true);
        $this->fileProductForm->getFileFieldset()->setFileInputAvailable(false);
        $this->fileProductForm->init();
        $this->fileProductForm->setData($data);
        $fileEntity = new FileEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->fileProductForm->isValid()) {
            $jsonModel->addMessages($this->fileProductForm->getMessages());
            return $jsonModel;
        }
        if (!$fileEntity->exchangeArrayFromDatabase($this->fileProductForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->fileProductRelService->updateFile($fileEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($file = $this->fileProductRelService->getFileProductRelJoined($id))) {
            $jsonModel->setObj($file);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
