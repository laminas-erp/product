<?php

namespace Lerp\Product\Factory\Controller\Rest\Workflow;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Product\Controller\Rest\Workflow\WorkflowController;
use Lerp\Product\Form\Workflow\WorkflowForm;
use Lerp\Product\Service\Workflow\WorkflowService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class WorkflowControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new WorkflowController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setWorkflowService($container->get(WorkflowService::class));
        $controller->setWorkflowForm($container->get(WorkflowForm::class));
        return $controller;
    }
}
