<?php

namespace Lerp\Product\Form;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ProductTextForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $langIsos = [];
    protected bool $isNew = false;

    public function init()
    {
        if (!$this->isNew) {
            $this->add(['name' => 'product_text_uuid']);
        } else {
            $this->add(['name' => 'product_uuid']);
        }
        $this->add(['name' => 'product_text_lang_iso']);
        $this->add(['name' => 'product_text_text_short']);
        $this->add(['name' => 'product_text_text_long']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if (!$this->isNew) {
            $filter['product_text_uuid'] = [
                'required' => true,
                'filters' => [['name' => StringTrim::class]],
                'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        } else {
            $filter['product_uuid'] = [
                'required' => true,
                'filters' => [['name' => StringTrim::class]],
                'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['product_text_lang_iso'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class]
            ], 'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => $this->langIsos
                    ]
                ]
            ]
        ];

        $filter['product_text_text_short'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 200,
                    ]
                ]
            ]
        ];

        $filter['product_text_text_long'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 64000,
                    ]
                ]
            ]
        ];

        return $filter;
    }

    /**
     * @param array $langIsos
     */
    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    /**
     * @param bool $isNew
     */
    public function setIsNew(bool $isNew): void
    {
        $this->isNew = $isNew;
    }

}
