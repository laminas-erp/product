<?php

namespace Lerp\Product\Service\Lists;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\Lists\ProductCclassTable;
use Lerp\Product\Table\Lists\ProductGroupTable;

class TablesService extends AbstractService
{
    protected ProductCclassTable $productCclassTable;
    protected ProductGroupTable $productGroupTable;

    public function setProductCclassTable(ProductCclassTable $productCclassTable): void
    {
        $this->productCclassTable = $productCclassTable;
    }

    public function setProductGroupTable(ProductGroupTable $productGroupTable): void
    {
        $this->productGroupTable = $productGroupTable;
    }

    /**
     * @return array
     */
    public function getListProductCclass(): array
    {
        return $this->productCclassTable->getProductCclassIdAssoc();
    }

    /**
     * @return array
     */
    public function getListProductGroup(): array
    {
        return $this->productGroupTable->getProductGroupIdAssoc();
    }

}
