<?php

namespace Lerp\Product\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Lerp\Product\Controller\Rest\ProductController;
use Lerp\Product\Form\ProductForm;
use Lerp\Product\Service\ProductService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 * @throws ContainerException if any other error occurs
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new ProductController();
		$controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductService($container->get(ProductService::class));
        $controller->setProductForm($container->get(ProductForm::class));
		return $controller;
	}
}
