<?php

namespace Lerp\Product\Factory\Controller\Stream;

use Bitkorn\Files\Service\FileService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Controller\Stream\FileStreamController;
use Lerp\Product\Service\Files\FileProductRelService;

class FileStreamControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FileStreamController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFileProductRelService($container->get(FileProductRelService::class));
        $controller->setFileService($container->get(FileService::class));
        $controller->setModuleBrand($container->get('config')['lerp_product']['module_brand']);
        return $controller;
    }
}
