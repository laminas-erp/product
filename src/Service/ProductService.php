<?php

namespace Lerp\Product\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Common\Service\ConfigService;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Product\Entity\ProductEntity;
use Lerp\Product\Table\ProductLocationTable;
use Lerp\Product\Table\ProductNoTable;
use Lerp\Product\Table\ProductSupplierTable;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Product\Table\ProductWorkflowTable;

class ProductService extends AbstractService
{
    protected ProductTable $productTable;
    protected ProductNoTable $productNoTable;
    protected ProductTextTable $productTextTable;
    protected ProductLocationTable $productLocationTable;
    protected ProductWorkflowTable $productWorkflowTable;
    protected ProductSupplierTable $productSupplierTable;
    protected QuantityUnitService $quantityUnitService;
    protected ConfigService $configService;
    protected array $langIsos;
    protected array $structures = [];
    protected array $origins = [];
    protected array $types = [];
    protected array $cclassIds = [];
    protected array $groupIds = [];
    protected int $searchProductCount = 0;

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductNoTable(ProductNoTable $productNoTable): void
    {
        $this->productNoTable = $productNoTable;
    }

    public function setProductTextTable(ProductTextTable $productTextTable): void
    {
        $this->productTextTable = $productTextTable;
    }

    public function setProductLocationTable(ProductLocationTable $productLocationTable): void
    {
        $this->productLocationTable = $productLocationTable;
    }

    public function setProductWorkflowTable(ProductWorkflowTable $productWorkflowTable): void
    {
        $this->productWorkflowTable = $productWorkflowTable;
    }

    public function setProductSupplierTable(ProductSupplierTable $productSupplierTable): void
    {
        $this->productSupplierTable = $productSupplierTable;
    }

    public function setQuantityUnitService(QuantityUnitService $quantityUnitService): void
    {
        $this->quantityUnitService = $quantityUnitService;
    }

    public function setConfigService(ConfigService $configService): void
    {
        $this->configService = $configService;
    }

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    public function setStructures(array $structures): void
    {
        $this->structures = $structures;
    }

    public function setOrigins(array $origins): void
    {
        $this->origins = $origins;
    }

    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    public function setCclassIds(array $cclassIds): void
    {
        $this->cclassIds = $cclassIds;
    }

    public function setGroupIds(array $groupIds): void
    {
        $this->groupIds = $groupIds;
    }

    /**
     * @param ProductEntity $productEntity
     * @return string
     */
    public function insertProduct(ProductEntity $productEntity): string
    {
        $productNoUuid = $this->generateProductNoNew()['product_no_uuid'];
        $productEntity->setProductNoUuid($productNoUuid);
        if (empty($productUuid = $this->productTable->insertProduct($productEntity))) {
            $this->productNoTable->deleteProductNo($productNoUuid);
        }
        if (!$this->productTextTable->insertProductTextCopies($productUuid, $this->langIsos)) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . ' can not create productTexts for product ' . $productUuid);
        }
        return $productUuid;
    }

    /**
     * @return array
     */
    protected function generateProductNoNew(): array
    {
        $productNo = $this->productNoTable->generateProductNoNew();
        if (empty($productNo) || !is_array($productNo) || empty($productNo['product_no_uuid'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() - Error while create new product no.');
        }
        return $productNo;
    }

    /**
     * @param ProductEntity $productEntity
     * @return bool
     */
    public function updateProduct(ProductEntity $productEntity): bool
    {
        return $this->productTable->updateProduct($productEntity) > 0;
    }

    public function updateProductBriefing(string $productUuid, string $productBriefing): bool
    {
        return $this->productTable->updateProductBriefing($productUuid, $productBriefing) >= 0;
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProduct(string $productUuid): array
    {
        return $this->productTable->getProduct($productUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductSimple(string $productUuid): array
    {
        return $this->productTable->getProductSimple($productUuid);
    }

    /**
     * @param string $productNoUuid
     * @return array
     */
    public function getProductsForNoUuid(string $productNoUuid): array
    {
        return $this->productTable->getProductsForNoUuid($productNoUuid);
    }

    /**
     * @param string $structure
     * @param string $origin
     * @param string $type
     * @param int $cclassId
     * @param int $groupId
     * @param string $productNo
     * @param string $text
     * @param string $dirty
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function searchProduct(string $structure, string $origin, string $type, int $cclassId, int $groupId, string $productNo, string $text, string $dirty, string $orderField, string $orderDirec, int $offset, int $limit): array
    {
        if ((
                !empty($structure) && !in_array($structure, $this->structures))
            || (!empty($origin) && !in_array($origin, $this->origins))
            || (!empty($type) && !in_array($type, $this->types))
            || (!empty($cclassId) && !array_key_exists($cclassId, $this->cclassIds))
            || (!empty($groupId) && !array_key_exists($groupId, $this->groupIds))
        ) {
            return [];
        }
        $productEntity = new ProductEntity();
        if (!$productEntity->fieldExistDatabase($orderField)) {
            $orderField = '';
        }
        if (!in_array($orderDirec, ['ASC', 'DESC'])) {
            $orderDirec = 'DESC';
        }
        $this->searchProductCount = $this->productTable->searchProduct($structure, $origin, $type, $cclassId, $groupId, $productNo, $text, $dirty, $orderField, $orderDirec, $offset, $limit, true)[0];
        return $this->productTable->searchProduct($structure, $origin, $type, $cclassId, $groupId, $productNo, $text, $dirty, $orderField, $orderDirec, $offset, $limit);
    }

    /**
     * @param string $productUuid
     * @return bool
     */
    public function computeChangeProductNo(string $productUuid): bool
    {
        $product = $this->productTable->getProduct($productUuid);
        if (empty($product)) {
            return false;
        }
        $countProductsNo = count($this->productTable->getProductsForNoUuid($product['product_no_uuid']));
        $productNoUuidNew = $this->generateProductNoNew()['product_no_uuid'];
        /** @var Adapter $adapter */
        $adapter = $this->productTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if ($this->productTable->updateProductNoUuid($productUuid, $productNoUuidNew) < 1) {
            $this->productNoTable->deleteProductNo($productNoUuidNew);
            return false;
        }
        if ($countProductsNo === 1) {
            if ($this->productNoTable->deleteProductNo($product['product_no_uuid']) < 1) {
                $connection->rollback();
                $this->productNoTable->deleteProductNo($productNoUuidNew);
                return false;
            }
        }
        $connection->commit();
        return true;
    }

    /**
     * @return int
     */
    public function getSearchProductCount(): int
    {
        return $this->searchProductCount;
    }

    /**
     * @param string $productUuid
     * @return array From db.view_location_case
     */
    public function getProductLocationsNotAssigned(string $productUuid): array
    {
        return $this->productLocationTable->getProductLocationsNotAssigned($productUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductLocations(string $productUuid): array
    {
        return $this->productLocationTable->getProductLocations($productUuid);
    }

    public function getProductLocation(string $productLocationUuid): array
    {
        return $this->productLocationTable->getProductLocation($productLocationUuid);
    }

    /**
     * @param string $productUuid
     * @param string $locationCaseUuid
     * @return bool
     */
    public function hasProductLocationCaseUuid(string $productUuid, string $locationCaseUuid): bool
    {
        return !empty($this->productLocationTable->getProductLocationForProductAndLocation($productUuid, $locationCaseUuid));
    }

    /**
     * @param string $productUuid
     * @param string $locationCaseUuid
     * @return string
     */
    public function insertProductLocation(string $productUuid, string $locationCaseUuid): string
    {
        return $this->productLocationTable->insertProductLocation($productUuid, $locationCaseUuid);
    }

    /**
     * @param string $productLocationUuid
     * @return bool
     */
    public function deleteProductLocation(string $productLocationUuid): bool
    {
        return $this->productLocationTable->deleteProductLocation($productLocationUuid) > 0;
    }

    /**
     * Checks if the $quantityunitUuid in the same quantityunit_resolution_group like the product.
     * @param string $productUuid
     * @param string $quantityunitUuid
     * @return bool
     */
    public function hasProductQuantityunit(string $productUuid, string $quantityunitUuid): bool
    {
        if (
            empty($product = $this->productTable->getProduct($productUuid))
            || empty($qu = $this->quantityUnitService->getQuantityUnit($quantityunitUuid))
            || $product['quantityunit_resolution_group'] != $qu['quantityunit_resolution_group']
        ) {
            return false;
        }
        return true;
    }

    /**
     * @param string $productNo
     * @return array
     */
    public function autocompleteProductNo(string $productNo): array
    {
        return $this->productNoTable->autocompleteProductNo($productNo);
    }

    /**
     * @param string $supplierUuid
     * @return array JOIN view_product
     */
    public function getSupplierProducts(string $supplierUuid): array
    {
        return $this->productSupplierTable->getSupplierProducts($supplierUuid);
    }

    /**
     * @param int $productNo
     * @return array The first product version with this $productNo.
     */
    public function getProductByNumber(int $productNo): array
    {
        return $this->productTable->getProductByNumber($productNo);
    }
}
