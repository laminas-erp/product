<?php

namespace Lerp\Product\Controller\Ajax\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Product\Form\Maint\ProductMaintWorkflowForm;
use Lerp\Product\Service\Maint\ProductMaintService;
use Laminas\Validator\Uuid;

class ProductMaintWorkflowController extends AbstractUserController
{
    protected ProductMaintWorkflowForm $productMaintWorkflowForm;
    protected ProductMaintService $productMaintService;

    public function setProductMaintWorkflowForm(ProductMaintWorkflowForm $productMaintWorkflowForm): void
    {
        $this->productMaintWorkflowForm = $productMaintWorkflowForm;
    }

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * GET all ProductMaintWorkflows for a ProductUUID.
     * @return JsonModel
     */
    public function allProductMaintWorkflowsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = filter_input(INPUT_GET, 'product_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (empty($productUuid) || !(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productMaints = $this->productMaintService->getProductMaintWorkflowsForProduct($productUuid))) {
            $jsonModel->setArr($productMaints);
            $jsonModel->setCount(count($productMaints));
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function updateProductMaintWorkflowOrderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productMaintWorkflowUuid = filter_input(INPUT_POST, 'product_maint_workflow_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $orderDirec = filter_input(INPUT_POST, 'order_direc', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($productMaintWorkflowUuid) || !in_array($orderDirec, ['up', 'down'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productMaintService->updateProductMaintWorkflowOrderPriority($productMaintWorkflowUuid, $orderDirec)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
