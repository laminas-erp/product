<?php

namespace Lerp\Product\Table\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductMaintDraftTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_maint_draft';

    /**
     * @param string $productMaintDraftUuid
     * @return array
     */
    public function getProductMaintDraft(string $productMaintDraftUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_maint_draft_uuid' => $productMaintDraftUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array All productMaintDrafts as uuid=>assoc array.
     */
    public function getProductMaintDraftsUuidAssoc(): array
    {
        $select = $this->sql->select();
        $uuidAssoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                foreach ($arr as $row) {
                    $uuidAssoc[$row['product_maint_draft_uuid']] = $row['product_maint_draft_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }

    /**
     * @return array All productMaintDrafts.
     */
    public function getProductMaintDrafts(): array
    {
        $select = $this->sql->select();
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $storage
     * @return string
     */
    public function insertProductMaintDraft(array $storage): string
    {
        $insert = $this->sql->insert();
        if (empty($storage)) {
            return '';
        }
        $uuid = $this->uuid();
        $storage['product_maint_draft_uuid'] = $uuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
