<?php

namespace Lerp\Product\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductEntity extends AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected array $mapping = [
        'product_uuid'                => 'product_uuid',
        'product_structure'           => 'product_structure',
        'product_origin'              => 'product_origin',
        'product_type'                => 'product_type',
        'product_cclass_id'           => 'product_cclass_id',
        'product_group_id'            => 'product_group_id',
        'product_text_part'           => 'product_text_part',
        'product_text_short'          => 'product_text_short',
        'product_text_long'           => 'product_text_long',
        'product_text_license'        => 'product_text_license',
        'product_text_license_remark' => 'product_text_license_remark',
        'product_briefing'            => 'product_briefing',
        'product_no_uuid'             => 'product_no_uuid',
        'product_uuid_origin'         => 'product_uuid_origin',
        'product_v1'                  => 'product_v1',
        'product_v2'                  => 'product_v2',
        'product_v3'                  => 'product_v3',
        'product_v_version'           => 'product_v_version',
        'product_v_revision'          => 'product_v_revision',
        'product_version_extern'      => 'product_version_extern',
        'product_revision_extern'     => 'product_revision_extern',
        'product_no_extern'           => 'product_no_extern',
        'product_spec_extern'         => 'product_spec_extern',
        'product_dirty'               => 'product_dirty',
        'quantityunit_uuid'           => 'quantityunit_uuid',
        'shop_product_uuid'           => 'shop_product_uuid',
        'product_time_create'         => 'product_time_create',
        'product_time_update'         => 'product_time_update',
        'product_taxp'                => 'product_taxp',
        // db.view_product
        'product_no_no'               => 'product_no_no',
        'product_calc_price_set'      => 'product_calc_price_set',
    ];

    protected $primaryKey = 'product_uuid';

    public function fieldExistDatabase(string $field): bool
    {
        return parent::fieldExistDatabase($field) || $field === 'product_no_no';
    }

    /**
     * @param string $productNoUuid
     */
    public function setProductNoUuid(string $productNoUuid)
    {
        if (empty($productNoUuid)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() - Empty $productNoUuid string');
        }
        $this->storage['product_no_uuid'] = $productNoUuid;
    }

    public function exchangeArray(array $data): bool
    {
        if (!parent::exchangeArray($data)) {
            return false;
        }
        if (!empty($this->storage['product_uuid'])) {
            $this->uuid = $this->storage['product_uuid'];
        }
        if (empty($this->storage['product_uuid_origin'])) {
            $this->storage['product_uuid_origin'] = null;
        }
        return true;
    }

    public function getStorage(): array
    {
        if (isset($this->storage['product_dirty'])) {
            $this->storage['product_dirty'] = boolval($this->storage['product_dirty']);
        }
        return $this->storage;
    }

    public function getNo(): string
    {
        if (!isset($this->storage['product_no_no'])) {
            return '';
        }
        return $this->storage['product_no_no'];
    }

    public function getStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function getOrigin(): string
    {
        if (!isset($this->storage['product_origin'])) {
            return '';
        }
        return $this->storage['product_origin'];
    }

    public function getType(): string
    {
        if (!isset($this->storage['product_type'])) {
            return '';
        }
        return $this->storage['product_type'];
    }

    public function getCclassId(): int
    {
        if (!isset($this->storage['product_cclass_id'])) {
            return 1;
        }
        return $this->storage['product_cclass_id'];
    }

    public function getGroupId(): int
    {
        if (!isset($this->storage['product_group_id'])) {
            return 1;
        }
        return $this->storage['product_group_id'];
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function setProductTextShort(string $productTextShort): void
    {
        $this->storage['product_text_short'] = $productTextShort;
    }

    public function getProductTextLong(): string
    {
        if (!isset($this->storage['product_text_long'])) {
            return '';
        }
        return $this->storage['product_text_long'];
    }

    public function setProductTextLong(string $productTextLong): void
    {
        $this->storage['product_text_long'] = $productTextLong;
    }

    public function getProductTextLicense(): string
    {
        if (!isset($this->storage['product_text_license'])) {
            return '';
        }
        return $this->storage['product_text_license'];
    }

    public function setProductTextLicense(string $productTextLicense): void
    {
        $this->storage['product_text_license'] = $productTextLicense;
    }

    public function getProductTextLicenseRemark(): string
    {
        if (!isset($this->storage['product_text_license_remark'])) {
            return '';
        }
        return $this->storage['product_text_license_remark'];
    }

    public function setProductTextLicenseRemark(string $productTextLicenseRemark): void
    {
        $this->storage['product_text_license_remark'] = $productTextLicenseRemark;
    }

    public function getVersionExtern(): string
    {
        if (!isset($this->storage['product_version_extern'])) {
            return '';
        }
        return $this->storage['product_version_extern'];
    }

    public function getRevisionExtern(): string
    {
        if (!isset($this->storage['product_revision_extern'])) {
            return '';
        }
        return $this->storage['product_revision_extern'];
    }

    public function getUuidOrigin(): string
    {
        if (!isset($this->storage['product_uuid_origin'])) {
            return '';
        }
        return $this->storage['product_uuid_origin'];
    }

    public function getQuantityUnitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityUnitUuid(string $quantityUnit): void
    {
        $this->storage['quantityunit_uuid'] = $quantityUnit;
    }

    public function getTaxp(): int
    {
        if (!isset($this->storage['product_taxp'])) {
            return 0;
        }
        return $this->storage['product_taxp'];
    }

    /**
     * unset() $this->storage: product_structure, product_origin, product_type
     */
    public function unsetStructuring(): void
    {
        unset($this->storage['product_structure']);
        unset($this->storage['product_origin']);
        unset($this->storage['product_type']);
    }

    /**
     * unset() $this->storage: product_cclass_id, product_group_id
     */
    public function unsetClassification(): void
    {
        unset($this->storage['product_cclass_id']);
        unset($this->storage['product_group_id']);
    }

    /**
     * @param float $productCalcPriceSet The price to use for selling.
     */
    public function setProductCalcPriceSet(float $productCalcPriceSet): void
    {
        $this->storage['product_calc_price_set'] = $productCalcPriceSet;
    }

    /**
     * @return float If product data come from db.view_product, this price ist the price to use.
     */
    public function getProductCalcPriceSet(): float
    {
        if (!isset($this->storage['product_calc_price_set'])) {
            return 0;
        }
        return $this->storage['product_calc_price_set'];
    }
}
