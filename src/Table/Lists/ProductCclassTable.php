<?php

namespace Lerp\Product\Table\Lists;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductCclassTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_cclass';


    /**
     * @param int $id
     * @return array
     */
    public function getProductCclass(int $id): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_cclass_id' => $id]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function getProductCclassIdAssoc(): array
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $row) {
                    $idAssoc[$row['product_cclass_id']] = $row['product_cclass_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }
}
