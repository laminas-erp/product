<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Lerp\Product\Entity\ProductEntity;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product';

    /**
     * @param string $productUuid
     * @return array From db.view_product
     */
    public function getProduct(string $productUuid): array
    {
        $select = new Select('view_product');
        try {
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param int $productNo
     * @return array The first product version with this $productNo.
     */
    public function getProductByNumber(int $productNo): array
    {
        $select = new Select('view_product');
        try {
            $select->where(['product_no_no' => $productNo]);
            // because, one product_no is used multiple times:
            $select->order('product_v1 ASC, product_v2 ASC, product_v3 ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductSimple(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $structure
     * @param string $origin
     * @param string $type
     * @param int $cclassId
     * @param int $groupId
     * @param string $productNo
     * @param string $text
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @param bool $doCount
     * @return array If $doCount then an array with one interger inside else the products.
     */
    public function searchProductAllVersions(string $structure, string $origin, string $type, int $cclassId, int $groupId, string $productNo, string $text, string $orderField, string $orderDirec, int $offset = 0, int $limit = 0, bool $doCount = false): array
    {
        $select = $this->sql->select();
        try {
            if (!$doCount) {
                $select->join('product_no', 'product_no.product_no_uuid = product.product_no_uuid', ['product_no_no'], Select::JOIN_LEFT);
            }

            if ($doCount) {
                $select->columns(['count_products' => new Expression('COUNT(product_uuid)')]);
            }

            if (!empty($structure)) {
                $select->where(['product_structure' => $structure]);
            }
            if (!empty($origin)) {
                $select->where(['product_origin' => $origin]);
            }
            if (!empty($type)) {
                $select->where(['product_type' => $type]);
            }
            if (!empty($cclassId)) {
                $select->where(['product_cclass_id' => $cclassId]);
            }
            if (!empty($groupId)) {
                $select->where(['product_group_id' => $groupId]);
            }

            if (!empty($productNo)) {
                $selectProductNo = new Select('product_no');
                $selectProductNo->columns(['product_no_uuid']);
                $selectProductNo->where->like(new Expression('CAST(product_no_no AS TEXT)'), "%$productNo%");
                $select->where->in('product.product_no_uuid', $selectProductNo);
            }
            if (!empty($text)) {
                $text = strtolower($text);
                $select->where->NEST->like(new Expression('LOWER(product_text_short)'), "%$text%")
                    ->or->like(new Expression('LOWER(product_text_long)'), "%$text%")
                    ->or->like(new Expression('LOWER(product_text_part)'), "%$text%");
            }
            if (!$doCount && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }

            if (!$doCount && (!empty($offset) || !empty($limit))) {
                $select->limit($limit)->offset($offset);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$doCount) {
                    return $result->toArray();
                } else {
                    return [intval($result->current()['count_products'])];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Search SQL View view_product_max_version
     *
     * @param string $structure
     * @param string $origin
     * @param string $type
     * @param int $cclassId
     * @param int $groupId
     * @param string $productNo
     * @param string $text
     * @param string $dirty
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @param bool $doCount
     * @return array
     */
    public function searchProduct(string $structure, string $origin, string $type, int $cclassId, int $groupId, string $productNo, string $text, string $dirty, string $orderField, string $orderDirec, int $offset = 0, int $limit = 0, bool $doCount = false): array
    {
        $select = new Select('view_product_max_version');
        try {
            if ($doCount) {
                $select->columns(['count_products' => new Expression('COUNT(product_uuid)')]);
            }

            if (!empty($structure)) {
                $select->where(['product_structure' => $structure]);
            }
            if (!empty($origin)) {
                $select->where(['product_origin' => $origin]);
            }
            if (!empty($type)) {
                $select->where(['product_type' => $type]);
            }
            if (!empty($cclassId)) {
                $select->where(['product_cclass_id' => $cclassId]);
            }
            if (!empty($groupId)) {
                $select->where(['product_group_id' => $groupId]);
            }

            if (!empty($productNo)) {
                $select->where->like(new Expression('CAST(product_no_no AS TEXT)'), "%$productNo%");
            }
            if (!empty($text)) {
                $text = strtolower($text);
                $select->where->NEST->like(new Expression('LOWER(product_text_short)'), "%$text%")
                    ->or->like(new Expression('LOWER(product_text_long)'), "%$text%")
                    ->or->like(new Expression('LOWER(product_text_part)'), "%$text%");
            }

            switch ($dirty) {
                case 'dirty':
                    $select->where(['product_dirty' => true]);
                    break;
                case 'clean':
                    $select->where(['product_dirty' => false]);
                    break;
            }

            if (!$doCount && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }

            if (!$doCount && (!empty($offset) || !empty($limit))) {
                $select->limit($limit)->offset($offset);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$doCount) {
                    return $result->toArray();
                } else {
                    return [intval($result->current()['count_products'])];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param ProductEntity $productEntity
     * @return string
     */
    public function insertProduct(ProductEntity $productEntity): string
    {
        $insert = $this->sql->insert();
        $storage = $productEntity->getStorage();
        if (empty($storage)) {
            return '';
        }
        $this->unsetNullFields($storage);
        $uuid = $this->uuid();
        $storage['product_uuid'] = $uuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     *
     * @param ProductEntity $productEntity
     * @return int
     */
    public function updateProduct(ProductEntity $productEntity): int
    {
        $update = $this->sql->update();
        $storage = $productEntity->getStorage();
        if (empty($storage)) {
            return '';
        }
        $this->unsetNullFields($storage);
        unset($storage['product_uuid']);
        unset($storage['product_no']);
        try {
            $update->set($storage);
            $update->where(['product_uuid' => $productEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productUuid
     * @param string $productNoUuid
     * @return int
     */
    public function updateProductNoUuid(string $productUuid, string $productNoUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['product_no_uuid' => $productNoUuid,]);
            $update->where(['product_uuid' => $productUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateProductBriefing(string $productUuid, string $productBriefing): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['product_briefing' => $productBriefing]);
            $update->where(['product_uuid' => $productUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productNoUuid
     * @return array
     */
    public function getProductsForNoUuid(string $productNoUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_no_uuid' => $productNoUuid]);
            $select->order('product_v1 ASC, product_v2 ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return int
     */
    public function deleteProduct(string $productUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_uuid' => $productUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}
