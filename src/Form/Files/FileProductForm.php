<?php

namespace Lerp\Product\Form\Files;

use Bitkorn\Files\Form\FileFieldset;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;

class FileProductForm extends AbstractForm implements InputFilterProviderInterface
{
    protected FileFieldset $fileFieldset;

    public function setFileFieldset(FileFieldset $fileFieldset): void
    {
        $this->fileFieldset = $fileFieldset;
    }

    public function getFileFieldset(): FileFieldset
    {
        return $this->fileFieldset;
    }

    public function init()
    {
        $this->add($this->fileFieldset);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->fileFieldset->getInputFilterSpecification();
    }
}
