<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductTextTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_text';

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductTextsForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            $select->order('product_text_lang_iso DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @param string $lang
     * @return array
     */
    public function getProductTextsForProductAndLang(string $productUuid, string $lang): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid, 'product_text_lang_iso' => $lang]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @param array $langIsos
     * @return bool
     */
    public function insertProductTextCopies(string $productUuid, array $langIsos): bool
    {
        if (empty($productUuid) || empty($langIsos)) {
            return false;
        }
        $insert = $this->sql->insert();
        try {
            $selectProductText = new Select('product');
            $selectProductText->columns(['product_uuid', 'product_text_short', 'product_text_long']);
            $selectProductText->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($selectProductText);
            if (!$result->valid() || $result->count() < 1) {
                return false;
            }
            $productText = $result->current();

            foreach ($langIsos as $langIso) {
                $uuid = $this->uuid();
                $insert->values([
                    'product_text_uuid' => $uuid,
                    'product_uuid' => $productUuid,
                    'product_text_lang_iso' => $langIso,
                    'product_text_text_short' => $productText['product_text_short'],
                    'product_text_text_long' => $productText['product_text_long'],
                ]);
                if ($this->insertWith($insert) < 1) {
                    return false;
                }
            }
            return true;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function insertProductTexts(string $productUuid, string $textShortDe, string $textLongDe, string $textShortEn, string $textLongEn): bool
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'product_text_uuid' => $this->uuid(),
                'product_uuid' => $productUuid,
                'product_text_lang_iso' => 'de',
                'product_text_text_short' => $textShortDe,
                'product_text_text_long' => $textLongDe,
            ]);
            if ($this->insertWith($insert) < 1) {
                return false;
            }
            $insert->values([
                'product_text_uuid' => $this->uuid(),
                'product_uuid' => $productUuid,
                'product_text_lang_iso' => 'en',
                'product_text_text_short' => $textShortEn,
                'product_text_text_long' => $textLongEn,
            ]);
            if ($this->insertWith($insert) < 1) {
                return false;
            }
            return true;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $productTextUuid
     * @param string $productTextShort
     * @param string $productTextLong
     * @return int
     */
    public function updateProductText(string $productTextUuid, string $productTextShort, string $productTextLong): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_text_text_short' => $productTextShort,
                'product_text_text_long' => $productTextLong
            ]);
            $update->where(['product_text_uuid' => $productTextUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productUuid
     * @param string $langIso
     * @param string $productTextShort
     * @param string $productTextLong
     * @return int
     */
    public function updateProductTextByProductUuid(string $productUuid, string $langIso, string $productTextShort, string $productTextLong): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_text_text_short' => $productTextShort,
                'product_text_text_long' => $productTextLong
            ]);
            $update->where(['product_uuid' => $productUuid, 'product_text_lang_iso' => $langIso]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productUuid
     * @return int
     */
    public function deleteProductTexts(string $productUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_uuid' => $productUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
