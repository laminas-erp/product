<?php

namespace Lerp\Product\Factory\Service;

use Interop\Container\ContainerInterface;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductListServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductListService();
        $service->setLogger($container->get('logger'));
        $service->setProductListTable($container->get(ProductListTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        return $service;
    }
}
