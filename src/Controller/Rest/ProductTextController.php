<?php

namespace Lerp\Product\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Product\Form\ProductTextForm;
use Lerp\Product\Service\ProductTextService;
use Laminas\Http\Response;

class ProductTextController extends AbstractUserRestController
{
    protected ProductTextForm $productTextForm;
    protected ProductTextService $productTextService;

    public function setProductTextForm(ProductTextForm $productTextForm): void
    {
        $this->productTextForm = $productTextForm;
    }

    public function setProductTextService(ProductTextService $productTextService): void
    {
        $this->productTextService = $productTextService;
    }

    /**
     * POST Create productText copy (from product) for Product_uuid.
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($data['product_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productTextService->insertProductTextCopy($data['product_uuid'])) {
            $jsonModel->setSuccess(1);
            return $jsonModel;
        }
        return $jsonModel;
    }

    /**
     * PUT updates all productTexts for a product.
     *
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $success = true;
        foreach ($data as $productText) {
            if (!$success) {
                break;
            }
            $this->productTextForm->init();
            $this->productTextForm->setData($productText);
            if (!$this->productTextForm->isValid()) {
                $jsonModel->addMessages($this->productTextForm->getMessages());
                $success = false;
                break;
            }
            $formData = $this->productTextForm->getData();
            if (!$this->productTextService->updateProductText($formData['product_text_uuid'], $formData['product_text_text_short'], $formData['product_text_text_long'])) {
                $success = false;
                break;
            }
        }
        if ($success) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET product texts by product_uuid.
     *
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->productTextService->getProductTextsForProduct($id));
        return $jsonModel;
    }
}
