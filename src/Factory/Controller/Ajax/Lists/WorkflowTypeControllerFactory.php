<?php

namespace Lerp\Product\Factory\Controller\Ajax\Lists;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Controller\Ajax\Lists\WorkflowTypeController;
use Lerp\Product\Service\Workflow\WorkflowService;

class WorkflowTypeControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new WorkflowTypeController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setWorkflowService($container->get(WorkflowService::class));
        return $controller;
    }
}
