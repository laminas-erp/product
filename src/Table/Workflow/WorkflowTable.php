<?php

namespace Lerp\Product\Table\Workflow;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Product\Entity\Workflow\WorkflowEntity;

class WorkflowTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'workflow';
    protected string $viewTable = 'view_workflow';

    /**
     * @param string $workflowUuid
     * @return array
     */
    public function getWorkflow(string $workflowUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['workflow_uuid' => $workflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $workflowLabel
     * @param bool $toLower
     * @return array
     */
    public function getWorkflowByLabel(string $workflowLabel, bool $toLower = false): array
    {
        $select = $this->sql->select();
        try {
            if ($toLower) {
                $select->where->equalTo(new Expression('LOWER(workflow_label)'), strtolower($workflowLabel));
            } else {
                $select->where(['workflow_label' => $workflowLabel]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existWorkflowByLabel(string $workflowLabel): bool
    {
        return !empty($this->getWorkflowByLabel($workflowLabel, true));
    }

    /**
     * @param string $workflowUuid
     * @return array
     */
    public function getWorkflowFromView(string $workflowUuid): array
    {
        $select = new Select($this->viewTable);
        try {
            $select->where(['workflow_uuid' => $workflowUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param bool $assoc
     * @return array
     */
    public function getWorkflows(bool $assoc): array
    {
        $select = new Select($this->viewTable);
        $uuidAssoc = [];
        try {
            $select->order('workflow_label ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$assoc) {
                    return $result->toArray();
                }
                do {
                    $current = $result->current();
                    $uuidAssoc[$current['workflow_uuid']] = $current['workflow_label'];
                    $result->next();
                } while ($result->valid());
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuidAssoc;
    }

    /**
     * @param WorkflowEntity $workflow
     * @return string
     */
    public function insertWorkflow(WorkflowEntity $workflow): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'workflow_uuid'           => $uuid,
                'workflow_label'          => $workflow->getWorkflowLabel(),
                'workflow_text'           => $workflow->getWorkflowText(),
                'workflow_price_per_hour' => $workflow->getWorkflowPricePerHour(),
                'workflow_code'           => $workflow->getWorkflowCode(),
                'workflow_is_extern'      => $workflow->getWorkflowIsExtern(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateWorkflow(WorkflowEntity $workflow): bool
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'workflow_label'          => $workflow->getWorkflowLabel(),
                'workflow_text'           => $workflow->getWorkflowText(),
                'workflow_price_per_hour' => $workflow->getWorkflowPricePerHour(),
                'workflow_code'           => $workflow->getWorkflowCode(),
                'workflow_is_extern'      => $workflow->getWorkflowIsExtern(),
            ]);
            $update->where(['workflow_uuid' => $workflow->getUuid()]);
            return $this->updateWith($update) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $workflowUuid
     * @return int
     */
    public function deleteWorkflow(string $workflowUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['workflow_uuid' => $workflowUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
