<?php

namespace Lerp\Product\Factory\Controller\Rest\Maint;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Controller\Rest\Maint\ProductMaintWorkflowController;
use Lerp\Product\Form\Maint\ProductMaintWorkflowForm;
use Lerp\Product\Service\Maint\ProductMaintService;

class ProductMaintWorkflowControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ProductMaintWorkflowController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductMaintWorkflowForm($container->get(ProductMaintWorkflowForm::class));
        $controller->setProductMaintService($container->get(ProductMaintService::class));
        return $controller;
    }
}
