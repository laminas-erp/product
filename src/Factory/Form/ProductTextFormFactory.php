<?php

namespace Lerp\Product\Factory\Form;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Product\Form\ProductTextForm;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductTextFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ProductTextForm();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        return $form;
    }
}
