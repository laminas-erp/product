<?php

namespace Lerp\Product\Form\Maint;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ProductMaintForm extends AbstractForm implements InputFilterProviderInterface
{

    /**
     * @var Adapter
     */
    protected $adapter;

    protected $productUuidRequired = true;

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    /**
     * @param bool $productUuidRequired
     */
    public function setProductUuidRequired(bool $productUuidRequired): void
    {
        $this->productUuidRequired = $productUuidRequired;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'product_maint_uuid']);
        }
        $this->add(['name' => 'product_uuid']);
        $this->add(['name' => 'product_maint_label']);
        $this->add(['name' => 'product_maint_desc']);
        $this->add(['name' => 'product_maint_order_priority']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['product_maint_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['product_uuid'] = [
            'required' => $this->productUuidRequired,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Uuid::class,
                ],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'product',
                        'field' => 'product_uuid'
                    ]
                ]
            ]
        ];

        $filter['product_maint_label'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['product_maint_desc'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        $filter['product_maint_order_priority'] = [
            'required' => false,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => Digits::class
                ]
            ]
        ];

        return $filter;
    }
}
