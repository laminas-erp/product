<?php

namespace Lerp\Product\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\ProductTextTable;
use Laminas\Log\Logger;

class ProductTextService extends AbstractService
{
    protected ProductTextTable $productTextTable;
    protected array $langIsos;

    public function setProductTextTable(ProductTextTable $productTextTable): void
    {
        $this->productTextTable = $productTextTable;
    }

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductTextsForProduct(string $productUuid): array
    {
        return $this->productTextTable->getProductTextsForProduct($productUuid);
    }

    public function getProductTextsForProductAndLang(string $productUuid, string $lang): array
    {
        return $this->productTextTable->getProductTextsForProductAndLang($productUuid, $lang);
    }

    public function insertProductTextCopy(string $productUuid): bool
    {
        if (!empty($this->productTextTable->getProductTextsForProduct($productUuid))) {
            return true;
        }
        return $this->productTextTable->insertProductTextCopies($productUuid, $this->langIsos);
    }

    public function updateProductText(string $productTextUuid, string $productTextShort, string $productTextLong): bool
    {
        return $this->productTextTable->updateProductText($productTextUuid, $productTextShort, $productTextLong) > 0;
    }
}
