<?php

namespace Lerp\Product\Factory\Service\Lists;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Lerp\Product\Service\Lists\EnumsService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class EnumsServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new EnumsService();
		$service->setLogger($container->get('logger'));
        $service->setAdapterDefault($container->get('dbDefault'));
		return $service;
	}
}
