<?php

namespace Lerp\Product\Controller\Ajax\Maint;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Product\Service\Maint\ProductMaintService;
use Laminas\Validator\Uuid;

class ProductMaintController extends AbstractUserController
{
    protected ProductMaintService $productMaintService;

    public function setProductMaintService(ProductMaintService $productMaintService): void
    {
        $this->productMaintService = $productMaintService;
    }

    /**
     * @return JsonModel
     */
    public function updateProductMaintOrderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productMaintUuid = filter_input(INPUT_POST, 'product_maint_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $orderDirec = filter_input(INPUT_POST, 'order_direc', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($productMaintUuid) || !in_array($orderDirec, ['up', 'down'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->productMaintService->updateProductMaintOrderPriority($productMaintUuid, $orderDirec)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
