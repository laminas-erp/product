<?php

namespace Lerp\Product\Entity\Maint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductMaintWorkflowEntity extends AbstractEntity
{
    public array $mapping = [
        'product_maint_workflow_uuid' => 'product_maint_workflow_uuid',
        'product_maint_uuid' => 'product_maint_uuid',
        'product_maint_workflow_label' => 'product_maint_workflow_label',
        'product_maint_workflow_desc' => 'product_maint_workflow_desc',
        'product_maint_workflow_order_priority' => 'product_maint_workflow_order_priority',
    ];
}
