<?php

namespace Lerp\Product\Factory\Controller\Rest\Maint;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Controller\Rest\Maint\ProductMaintController;
use Lerp\Product\Controller\Rest\Maint\ProductMaintDraftController;
use Lerp\Product\Form\Maint\ProductMaintDraftForm;
use Lerp\Product\Form\Maint\ProductMaintForm;
use Lerp\Product\Service\Maint\ProductMaintService;

class ProductMaintDraftControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ProductMaintDraftController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductMaintDraftForm($container->get(ProductMaintDraftForm::class));
        $controller->setProductMaintService($container->get(ProductMaintService::class));
        return $controller;
    }
}
