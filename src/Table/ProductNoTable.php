<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductNoTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_no';

    /**
     * @param string $productNoUuid
     * @return array
     */
    public function getProductNo(string $productNoUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_no_uuid' => $productNoUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() === 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @return array
     */
    public function generateProductNoNew(): array
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values(['product_no_uuid' => $uuid]);
            if ($this->insertWith($insert) < 1) {
                return [];
            }
            return $this->getProductNo($uuid);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productNoUuid
     * @return int
     */
    public function deleteProductNo(string $productNoUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_no_uuid' => $productNoUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function existProductNoNo(int $productNoNo): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_no_no' => $productNoNo]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() === 1) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $productNoNo
     * @return array
     */
    public function autocompleteProductNo(string $productNoNo): array
    {
        $select = new Select('view_product');
        try {
            $select->columns(['product_uuid', 'product_text_part', 'product_text_short', 'product_no_no']);
            $select->where->like(new Expression('CAST(product_no_no AS TEXT)'), '%' . $productNoNo . '%');
            $select->order('product_no_no ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
