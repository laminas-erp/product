<?php

namespace Lerp\Product\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\ProductWorkflowTable;
use Lerp\Product\Table\Workflow\WorkflowTable;

class ProductWorkflowService extends AbstractService
{
    protected ProductWorkflowTable $productWorkflowTable;
    protected WorkflowTable $workflowTable;

    public function setProductWorkflowTable(ProductWorkflowTable $productWorkflowTable): void
    {
        $this->productWorkflowTable = $productWorkflowTable;
    }

    public function setWorkflowTable(WorkflowTable $workflowTable): void
    {
        $this->workflowTable = $workflowTable;
    }

    /**
     * @param string $productUuid
     * @param string $workflowType One entry from db.enum_workflow_type('craft', 'service', 'misc')
     * @param bool|null $extern If isset then use it for WHERE workflow_is_extern
     * @return array
     */
    public function getProductWorkflows(string $productUuid, string $workflowType = '', bool $extern = null): array
    {
        return $this->productWorkflowTable->getProductWorkflows($productUuid, $workflowType, $extern);
    }

    /**
     * @param string $productUuid
     * @param string $workflowUuid
     * @param float $pricePerHour
     * @return string
     */
    public function insertProductWorkflow(string $productUuid, string $workflowUuid, float $pricePerHour, int $workflowTime = 0): string
    {
        return $this->productWorkflowTable->insertProductWorkflow($productUuid, $workflowUuid, $pricePerHour,
            $this->workflowTable->getWorkflow($workflowUuid)['workflow_text'], $workflowTime);
    }

    /**
     * @param string $workflowUuid
     * @return bool
     */
    public function deleteWorkflow(string $workflowUuid): bool
    {
        return $this->productWorkflowTable->deleteProductWorkflow($workflowUuid) > 0;
    }

    /**
     * @param string $productWorkflowUuid
     * @param string $workflowUuid
     * @param int $workflowTime
     * @param float $pricePerHour
     * @param int $orderPriority
     * @param string $workflowText
     * @return bool
     */
    public function updateProductWorkflow(string $productWorkflowUuid, string $workflowUuid, int $workflowTime, float $pricePerHour, int $orderPriority, string $workflowText): bool
    {
        return $this->productWorkflowTable->updateProductWorkflow($productWorkflowUuid, $workflowUuid, $workflowTime, $pricePerHour, $orderPriority, $workflowText) >= 0;
    }

    /**
     * @param string $workflowUuid
     * @param string $direc
     * @return bool
     */
    public function updateWorkflowOrderPriority(string $workflowUuid, string $direc): bool
    {
        return $this->productWorkflowTable->updateProductWorkflowOrderPriority($workflowUuid, $direc) >= 0;
    }
}
