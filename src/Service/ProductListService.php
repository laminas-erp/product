<?php

namespace Lerp\Product\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Entity\ProductListEntity;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductTable;

class ProductListService extends AbstractService
{
    protected ProductListTable $productListTable;
    protected ProductTable $productTable;

    /**
     * @var ProductListEntity[]
     */
    protected array $productListEntities;

    /**
     * @var ProductListEntity With all children!
     */
    protected ProductListEntity $productListEntity;

    public function setProductListTable(ProductListTable $productListTable): void
    {
        $this->productListTable = $productListTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    /**
     * Product list with join to some product tables.
     *
     * @param string $productUuid
     * @return array
     */
    public function getProductListRecursive(string $productUuid): array
    {
        return $this->productListTable->getProductListRecursive($productUuid);
    }

    public function validInsertProductList(string $productUuidParent, string $productUuid): bool
    {
        $productParent = $this->productTable->getProductSimple($productUuidParent);
        if ($productUuidParent == $productUuid) {
            $this->message = 'Parent Produkt ist gleich Produkt';
            return false;
        }
        if ($productParent['product_structure'] !== 'list') {
            $this->message = 'Parent Produkt nicht vom Typ Liste';
            return false;
        }
        if (in_array($productUuid, $this->getProductListRecursiveParents($productUuidParent))) {
            $this->message = 'Produkt als Parent vorhanden';
            return false;
        }
        if ($this->productListTable->existProductListItem($productUuidParent, $productUuid)) {
            $this->message = 'Produkt als Neighbor vorhanden';
            return false;
        }
        return true;
    }

    /**
     * @param string $productUuidParent
     * @param string $productUuid
     * @param float $quantity
     * @return string
     */
    public function insertProductList(string $productUuidParent, string $productUuid, float $quantity): string
    {
        return $this->productListTable->insertProductListItem($productUuidParent, $productUuid, $quantity);
    }

    /**
     * @param string $productListUuid
     * @return bool
     */
    public function deleteProductListItem(string $productListUuid): bool
    {
        return $this->productListTable->deleteProductListItem($productListUuid) > 0;
    }

    /**
     * @param string $productUuidParent
     * @return array Array with product_uuid's
     */
    public function getProductListRecursiveParents(string $productUuidParent): array
    {
        return $this->productListTable->getProductListRecursiveParents($productUuidParent);
    }

    /**
     * @param string $productListUuid
     * @param string $direc
     * @return bool TRUE if nothing to update.
     */
    public function updateListItemOrderPriority(string $productListUuid, string $direc): bool
    {
        $neighbors = $this->productListTable->getProductListItemsByListUuid($productListUuid);
        if (empty($neighbors)) {
            return false;
        }
        foreach ($neighbors as $i => $neighbor) {
            if ($neighbor['product_list_uuid'] == $productListUuid) {
                $self = $neighbor;
                $top = isset($neighbors[$i - 1]) ? $neighbors[$i - 1] : null;
                $down = isset($neighbors[$i + 1]) ? $neighbors[$i + 1] : null;
            }
        }
        if ($direc === 'up') {
            if (!isset($top)) {
                return false;
            }
            $this->productListTable->updateProductListItemOrderPriority($top['product_list_uuid'], $self['product_list_order_priority']);
            $this->productListTable->updateProductListItemOrderPriority($self['product_list_uuid'], $top['product_list_order_priority']);
        } else if ($direc === 'down') {
            if (!isset($down)) {
                return false;
            }
            $this->productListTable->updateProductListItemOrderPriority($down['product_list_uuid'], $self['product_list_order_priority']);
            $this->productListTable->updateProductListItemOrderPriority($self['product_list_uuid'], $down['product_list_order_priority']);
        }
        return true;
    }

    /**
     * @param string $productListUuid
     * @param float $quantity
     * @return bool
     */
    public function updateListItemQuantity(string $productListUuid, float $quantity): bool
    {
        return $this->productListTable->updateProductListItemQuantity($productListUuid, $quantity) > 0;
    }
}
