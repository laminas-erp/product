<?php

namespace Lerp\Product\Entity\Maint;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductMaintDraftEntity extends AbstractEntity
{
    public array $mapping = [
        'product_maint_draft_uuid' => 'product_maint_draft_uuid',
        'product_maint_draft_label' => 'product_maint_draft_label',
        'product_maint_draft_desc' => 'product_maint_draft_desc',
    ];
}
