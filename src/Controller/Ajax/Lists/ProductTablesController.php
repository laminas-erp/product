<?php

namespace Lerp\Product\Controller\Ajax\Lists;

use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Product\Service\Lists\TablesService;
use Laminas\Http\Response;
use Bitkorn\Trinket\View\Model\JsonModel;

class ProductTablesController extends AbstractUserController
{
    protected TablesService $tablesService;

    public function setTablesService(TablesService $tablesService): void
    {
        $this->tablesService = $tablesService;
    }

    /**
     * @return JsonModel
     */
    public function productCclassAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->tablesService->getListProductCclass());
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function productGroupAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->tablesService->getListProductGroup());
        return $jsonModel;
    }
}
