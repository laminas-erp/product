<?php

namespace Lerp\Product\Table\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FileProductRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_product_rel';

    protected array $viewProductColumns = ['product_structure', 'product_origin', 'product_type', 'product_text_part', 'product_text_short', 'product_no_no'];

    /**
     * @param string $fileProductRelUuid
     * @return array
     */
    public function getFileProductRel(string $fileProductRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_product_rel_uuid' => $fileProductRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileProductRel(string $fileProductRelUuid): bool
    {
        return !empty($this->getFileProductRel($fileProductRelUuid));
    }

    /**
     * @param string $fileProductRelUuid
     * @return array
     */
    public function getFileProductRelJoined(string $fileProductRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_product_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_product_rel_uuid' => $fileProductRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileProductRelForFileAndProduct(string $fileUuid, string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileProductRelForFileAndProduct(string $fileUuid, string $productUuid): bool
    {
        $file = $this->getFileProductRelForFileAndProduct($fileUuid, $productUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_product_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFilesForProducts(array $productUuids): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_product_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->join('view_product', 'view_product.product_uuid = file_product_rel.product_uuid'
                , $this->viewProductColumns
                , Select::JOIN_LEFT);
            $select->where->in('file_product_rel.product_uuid', $productUuids);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFilesForProductsWithSelect(Select $productUuidSelect): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_product_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->join('view_product', 'view_product.product_uuid = file_product_rel.product_uuid'
                , $this->viewProductColumns
                , Select::JOIN_LEFT);
            $select->where->in('file_product_rel.product_uuid', $productUuidSelect);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileProductRel(string $fileUuid, string $productUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_product_rel_uuid' => $uuid,
                'file_uuid'             => $fileUuid,
                'product_uuid'          => $productUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $fileProductRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_product_rel_uuid' => $fileProductRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $productUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFileProductRelForFileAndProduct($fileUuid, $productUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}
