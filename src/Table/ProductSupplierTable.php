<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductSupplierTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_supplier';

    public function getProductSupplier(string $productSupplierId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_supplier_id' => $productSupplierId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array All product_supplier relations for one product - ordered by product_supplier_rating DESC
     */
    public function getProductSuppliers(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            $select->order('product_supplier_rating DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $supplierUuid
     * @return array JOIN view_product
     */
    public function getSupplierProducts(string $supplierUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('view_product', 'view_product.product_uuid=product_supplier.product_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['supplier_uuid' => $supplierUuid]);
            $select->order('product_supplier_rating DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertProductSupplier(string $productUuid, string $supplierUuid, int $productSupplierRating = 0): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'product_uuid'            => $productUuid,
                'supplier_uuid'           => $supplierUuid,
                'product_supplier_rating' => $productSupplierRating,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.product_supplier_product_supplier_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function existProductSupplierRelation(string $productUuid, string $supplierUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid, 'supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }
}
