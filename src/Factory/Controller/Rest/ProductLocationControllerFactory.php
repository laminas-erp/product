<?php

namespace Lerp\Product\Factory\Controller\Rest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Location\Service\LocationService;
use Lerp\Product\Controller\Rest\ProductLocationController;
use Lerp\Product\Service\ProductService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Stock\Service\StockService;

class ProductLocationControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ProductLocationController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setProductService($container->get(ProductService::class));
        $controller->setLocationService($container->get(LocationService::class));
        $controller->setStockService($container->get(StockService::class));
        return $controller;
    }
}
