<?php

namespace Lerp\Product\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Location\Service\LocationService;
use Lerp\Product\Service\ProductService;
use Laminas\Http\Response;
use Lerp\Stock\Service\StockService;

class ProductLocationController extends AbstractUserRestController
{
    protected ProductService $productService;
    protected LocationService $locationService;
    protected StockService $stockService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setLocationService(LocationService $locationService): void
    {
        $this->locationService = $locationService;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    /**
     * GET All product-locations for one product_uuid.
     * Query param "assigned"
     * - != 1 = Get product locations which are not assigned to the product
     * - == 1 = Get product locations which are assigned to the product
     *
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->params()->fromQuery('assigned') == 1) {
            $jsonModel->setArr($this->productService->getProductLocations($id));
        } else {
            $jsonModel->setArr($this->productService->getProductLocationsNotAssigned($id));
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = filter_input(INPUT_POST, 'product_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $locationCaseUuid = filter_input(INPUT_POST, 'location_case_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (!(new Uuid())->isValid($productUuid) || empty($locationCaseUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productLocationUuid = $this->productService->insertProductLocation($productUuid, $locationCaseUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setVariable('product_location_uuid', $productLocationUuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id product_location_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $pl = $this->productService->getProductLocation($id);
        /**
         * @todo die GUI braucht ne Funktion, um ein Produkt von einem Lagerplatz in ein anderes zu schieben. Also, alle stockin mit neuer location_case_uuid updaten.
         */
        if (empty($pl) || $this->stockService->existStockinForProductAndLocationCase($pl['product_uuid'], $pl['location_case_uuid'])) {
            $jsonModel->addMessage('Der Produkt-Lagerplatz kann nicht gelöscht werden');
            return $jsonModel;
        }
        if ($this->productService->deleteProductLocation($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
