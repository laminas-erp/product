<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\PredicateSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductLocationTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_location';

    public function getProductLocation(string $productLocationUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_location_uuid' => $productLocationUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductLocations(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns(['product_location_id', 'product_uuid']);
            $select->join('view_location_case', 'view_location_case.location_case_uuid = product_location.location_case_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['product_uuid' => $productUuid]);
            $select->order('location_place_id ASC, location_room_id ASC, location_rack_id ASC, location_row_id ASC, location_case_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array From db.view_location_case
     */
    public function getProductLocationsNotAssigned(string $productUuid): array
    {
        $select = new Select('view_location_case');
        try {
            $selectCaseUuids = $this->sql->select();
            $selectCaseUuids->columns(['location_case_uuid']);
            $selectCaseUuids->where(['product_uuid' => $productUuid]);

            $select->where->notIn('location_case_uuid', $selectCaseUuids);
            $select->order('location_place_id ASC, location_room_id ASC, location_rack_id ASC, location_row_id ASC, location_case_id ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getProductLocationForProductAndLocation(string $productUuid, string $locationCaseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'product_uuid' => $productUuid,
                'location_case_uuid' => $locationCaseUuid
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertProductLocation(string $productUuid, string $locationCaseUuid): string
    {
        $productLocation = $this->getProductLocationForProductAndLocation($productUuid, $locationCaseUuid);
        if (!empty($productLocation)) {
            return $productLocation['product_location_id'];
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'product_uuid' => $productUuid,
                'location_case_uuid' => $locationCaseUuid
            ]);
            if ($this->insertWith($insert) > 0) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.product_location_product_location_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $productLocationUuid
     * @return int
     */
    public function deleteProductLocation(string $productLocationUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where([
                'product_location_uuid' => $productLocationUuid
            ]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
