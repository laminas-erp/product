<?php

namespace Lerp\Product\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductListEntity extends AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected array $mapping = [
        'product_list_uuid' => 'product_list_uuid',
        'product_uuid_top' => 'product_uuid_top',
        'product_uuid_parent' => 'product_uuid_parent',
        'product_uuid' => 'product_uuid',
        'product_list_quantity' => 'product_list_quantity',
        // product

    ];

    /**
     *
     * @var ProductListEntity
     */
    protected $parent;

    /**
     *
     * @var ProductListEntity[]
     */
    protected $children;

    /**
     *
     * @param ProductListEntity $parent
     */
    public function setParent(ProductListEntity $parent)
    {
        $this->parent = $parent;
    }

    /**
     *
     * @param ProductListEntity $child
     */
    public function addChild(ProductListEntity $child)
    {
        $this->children[] = $child;
    }

    /**
     *
     * @return ProductListEntity
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     *
     * @return boolean
     */
    public function hasParent()
    {
        return !empty($this->parent) && $this->parent instanceof ProductListEntity;
    }

    /**
     *
     * @return boolean
     */
    public function hasChildren()
    {
        return !empty($this->children[0]) && $this->children[0] instanceof ProductListEntity;
    }

    /**
     *
     * @return ProductListEntity[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function computeQuantityComplete()
    {
        if ($this->hasParent()) {
            return $this->storage['product_list_quantity'] * $this->getParent()->computeQuantityComplete();
        }
        return $this->storage['product_list_quantity'];
    }

    public function setQuantity(int $anzahl)
    {
        $this->storage['product_list_quantity'] = $anzahl;
    }

}
