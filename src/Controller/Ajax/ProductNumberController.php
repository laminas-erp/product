<?php

namespace Lerp\Product\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Lerp\Product\Service\ProductService;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class ProductNumberController extends AbstractUserController
{
    protected ProductService $productService;

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    /**
     * A God function!
     * Generate new product-number for one product.
     * @return JsonModel
     */
    public function computeChangeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }

        $uuid = $this->params('uuid');
        if (empty($uuid) || !(new Uuid())->isValid($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!$this->productService->computeChangeProductNo($uuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function autocompleteAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $number = $this->params('number');
        if (empty($number) || empty($products = $this->productService->autocompleteProductNo($number))) {
            return $jsonModel;
        }
        $termResult = [];
        foreach ($products as $product) {
            $obj = new \stdClass();
            $obj->label = $product['product_no_no']
                . ' - ' . (!empty($product['product_text_part']) ? $product['product_text_part'] . '|#|' : '')
                . $product['product_text_short'];
            $obj->value = $product['product_no_no'];
            $termResult[] = $obj;
        }
        $jsonModel->setArr($termResult);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function autocompleteReturnUuidAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $number = $this->params('number');
        if (empty($number) || empty($products = $this->productService->autocompleteProductNo($number))) {
            return $jsonModel;
        }
        $termResult = [];
        foreach ($products as $product) {
            $obj = new \stdClass();
            $obj->label = $product['product_no_no']
                . ' - ' . (!empty($product['product_text_part']) ? $product['product_text_part'] . '|#|' : '')
                . $product['product_text_short'];
            $obj->value = $product['product_uuid'];
            $obj->value2 = $product['product_no_no'];
            $termResult[] = $obj;
        }
        $jsonModel->setArr($termResult);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function productByNumberAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $number = $this->params('number');
        if (empty($number) || empty($product = $this->productService->getProductByNumber($number))) {
            return $jsonModel;
        }
        $jsonModel->setObj($product);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
