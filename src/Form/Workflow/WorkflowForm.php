<?php

namespace Lerp\Product\Form\Workflow;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

/**
 * Class WorkflowForm
 * @package Lerp\Product\Form\Workflow
 */
class WorkflowForm extends AbstractForm implements InputFilterProviderInterface
{

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'workflow_uuid']);
        }
        //$this->add(['name' => 'workflow_type']);
        $this->add(['name' => 'workflow_label']);
        $this->add(['name' => 'workflow_text']);
        $this->add(['name' => 'workflow_price_per_hour']);
        $this->add(['name' => 'workflow_code']);
        $this->add(['name' => 'workflow_is_extern']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['workflow_uuid'] = [
                'required'   => true,
                'filters'    => [['name' => FilterChainStringSanitize::class]],
                'validators' => [['name' => Uuid::class]]
            ];
        }

        $filter['workflow_label'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class],],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 3,
                        'max'      => 160,
                    ]
                ]
            ]
        ];

        $filter['workflow_text'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class],],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        $filter['workflow_price_per_hour'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class],],
            'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['workflow_code'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class],],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 20,
                    ]
                ]
            ]
        ];

        $filter['workflow_is_extern'] = [
            'required'          => true,
            'continue_if_empty' => true,
            'filters'           => [
                [
                    'name'    => Boolean::class,
                    'options' => [
                        'type' => Boolean::TYPE_ALL
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
