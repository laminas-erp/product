<?php

namespace Lerp\Product;

use Lerp\Product\Controller\Ajax\Lists\ProductEnumsController;
use Lerp\Product\Controller\Ajax\Lists\ProductTablesController;
use Lerp\Product\Controller\Ajax\Lists\ProductTaxController;
use Lerp\Product\Controller\Ajax\Lists\WorkflowTypeController;
use Lerp\Product\Controller\Ajax\ProductNumberController;
use Lerp\Product\Controller\Ajax\ProductVersionController;
use Lerp\Product\Controller\Ajax\SupplierProductAjaxController;
use Lerp\Product\Controller\Ajax\Workflow\ProductWorkflowAjaxController;
use Lerp\Product\Controller\Rest\Files\FileProductRestController;
use Lerp\Product\Controller\Rest\Maint\ProductMaintController;
use Lerp\Product\Controller\Rest\Maint\ProductMaintDraftController;
use Lerp\Product\Controller\Rest\Maint\ProductMaintWorkflowController;
use Lerp\Product\Controller\Rest\ProductList\ProductListRestController;
use Lerp\Product\Controller\Rest\ProductList\ProductListItemRestController;
use Lerp\Product\Controller\Rest\ProductLocationController;
use Lerp\Product\Controller\Rest\ProductTextController;
use Lerp\Product\Controller\Rest\ProductWorkflowRestController;
use Lerp\Product\Controller\Rest\Workflow\WorkflowController;
use Lerp\Product\Controller\Stream\FileStreamController;
use Lerp\Product\Factory\Controller\Ajax\Lists\ProductEnumsControllerFactory;
use Lerp\Product\Controller\Rest\ProductController;
use Lerp\Product\Factory\Controller\Ajax\Lists\ProductTablesControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\Lists\ProductTaxControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\Lists\WorkflowTypeControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\ProductNumberControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\ProductVersionControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\SupplierProductAjaxControllerFactory;
use Lerp\Product\Factory\Controller\Ajax\Workflow\ProductWorkflowAjaxControllerFactory;
use Lerp\Product\Factory\Controller\Rest\Files\FileProductRestControllerFactory;
use Lerp\Product\Factory\Controller\Rest\Maint\ProductMaintControllerFactory;
use Lerp\Product\Factory\Controller\Rest\Maint\ProductMaintDraftControllerFactory;
use Lerp\Product\Factory\Controller\Rest\Maint\ProductMaintWorkflowControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductList\ProductListRestControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductList\ProductListItemRestControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductLocationControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductTextControllerFactory;
use Lerp\Product\Factory\Controller\Rest\ProductWorkflowRestControllerFactory;
use Lerp\Product\Factory\Controller\Rest\Workflow\WorkflowControllerFactory;
use Lerp\Product\Factory\Controller\Stream\FileStreamControllerFactory;
use Lerp\Product\Factory\Form\Files\FileProductFormFactory;
use Lerp\Product\Factory\Form\Maint\ProductMaintDraftFormFactory;
use Lerp\Product\Factory\Form\Maint\ProductMaintFormFactory;
use Lerp\Product\Factory\Form\Maint\ProductMaintWorkflowFormFactory;
use Lerp\Product\Factory\Form\ProductFormFactory;
use Lerp\Product\Factory\Form\ProductTextFormFactory;
use Lerp\Product\Factory\Form\ProductWorkflowFormFactory;
use Lerp\Product\Factory\Form\Workflow\WorkflowFormFactory;
use Lerp\Product\Factory\Service\Calc\ProductCalcServiceFactory;
use Lerp\Product\Factory\Service\Files\FileProductRelServiceFactory;
use Lerp\Product\Factory\Service\Lists\EnumsServiceFactory;
use Lerp\Product\Factory\Service\Lists\TablesServiceFactory;
use Lerp\Product\Factory\Service\Maint\ProductMaintServiceFactory;
use Lerp\Product\Factory\Service\ProductGodServiceFactory;
use Lerp\Product\Factory\Service\ProductListServiceFactory;
use Lerp\Product\Factory\Service\ProductServiceFactory;
use Lerp\Product\Factory\Service\ProductTextServiceFactory;
use Lerp\Product\Factory\Service\ProductWorkflowServiceFactory;
use Lerp\Product\Factory\Service\Workflow\WorkflowServiceFactory;
use Lerp\Product\Factory\Table\Files\FileProductRelTableFactory;
use Lerp\Product\Factory\Table\Lists\ProductCclassTableFactory;
use Lerp\Product\Factory\Table\Lists\ProductGroupTableFactory;
use Lerp\Product\Factory\Table\Maint\ProductMaintDraftTableFactory;
use Lerp\Product\Factory\Table\Maint\ProductMaintTableFactory;
use Lerp\Product\Factory\Table\Maint\ProductMaintWorkflowTableFactory;
use Lerp\Product\Factory\Table\ProductListTableFactory;
use Lerp\Product\Factory\Table\ProductLocationTableFactory;
use Lerp\Product\Factory\Table\ProductNoTableFactory;
use Lerp\Product\Factory\Table\ProductSupplierTableFactory;
use Lerp\Product\Factory\Table\ProductTableFactory;
use Lerp\Product\Factory\Table\ProductTextTableFactory;
use Lerp\Product\Factory\Table\ProductWorkflowTableFactory;
use Lerp\Product\Factory\Table\Workflow\WorkflowEquipmentGroupRelTableFactory;
use Lerp\Product\Factory\Table\Workflow\WorkflowTableFactory;
use Lerp\Product\Form\Files\FileProductForm;
use Lerp\Product\Form\Maint\ProductMaintDraftForm;
use Lerp\Product\Form\Maint\ProductMaintForm;
use Lerp\Product\Form\Maint\ProductMaintWorkflowForm;
use Lerp\Product\Form\ProductForm;
use Lerp\Product\Form\ProductTextForm;
use Lerp\Product\Form\ProductWorkflowForm;
use Lerp\Product\Form\Workflow\WorkflowForm;
use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\Product\Service\Files\FileProductRelService;
use Lerp\Product\Service\Lists\EnumsService;
use Lerp\Product\Service\Lists\TablesService;
use Lerp\Product\Service\Maint\ProductMaintService;
use Lerp\Product\Service\ProductGodService;
use Lerp\Product\Service\ProductListService;
use Lerp\Product\Service\ProductService;
use Lerp\Product\Service\ProductTextService;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Product\Service\Workflow\WorkflowService;
use Lerp\Product\Table\Files\FileProductRelTable;
use Lerp\Product\Table\Lists\ProductCclassTable;
use Lerp\Product\Table\Lists\ProductGroupTable;
use Lerp\Product\Table\Maint\ProductMaintDraftTable;
use Lerp\Product\Table\Maint\ProductMaintTable;
use Lerp\Product\Table\Maint\ProductMaintWorkflowTable;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductLocationTable;
use Lerp\Product\Table\ProductNoTable;
use Lerp\Product\Table\ProductSupplierTable;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Product\Table\ProductWorkflowTable;
use Lerp\Product\Table\Workflow\WorkflowEquipmentGroupRelTable;
use Lerp\Product\Table\Workflow\WorkflowTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router'          => [
        'routes' => [
            /*
             * REST - Product
             */
            'lerp_product_rest_product_product'                               => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-product[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductController::class,
                    ],
                ],
            ],
            'lerp_product_rest_product_productlocation'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-product-location[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductLocationController::class,
                    ],
                ],
            ],
            'lerp_product_rest_product_producttext'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-product-text[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductTextController::class,
                    ],
                ],
            ],
            'lerp_product_rest_workflow_productworkflow'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-rest-productworkflow[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductWorkflowRestController::class,
                    ],
                ],
            ],
            /*
             * REST - ProductList
             */
            'lerp_product_rest_product_productlist'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-product-list[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductListRestController::class,
                    ],
                ],
            ],
            'lerp_product_rest_product_productlist_item'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-rest-product-list-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductListItemRestController::class,
                    ],
                ],
            ],
            /*
             * REST - Product Maintenance
             */
            'lerp_product_rest_maint_productmaint'                            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-maint-product-maint[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductMaintController::class,
                    ],
                ],
            ],
            'lerp_product_rest_maint_productmaintdraft'                       => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-maint-product-maint-draft[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductMaintDraftController::class,
                    ],
                ],
            ],
            'lerp_product_rest_maint_productmaintworkflow'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-maint-product-maint-workflow[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductMaintWorkflowController::class,
                    ],
                ],
            ],
            /*
             * REST - Workflow
             */
            'lerp_product_rest_workflow_workflow'                             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-workflow[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => WorkflowController::class,
                    ],
                ],
            ],
            /*
             * REST - Files
             */
            'lerp_product_rest_fileproduct'                                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-product[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileProductRestController::class,
                    ],
                ],
            ],
            /**
             * Stream
             */
            'lerp_product_stream_filestream_stream'                           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileStreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
            /*
             * AJAX - ProductNumber
             */
            'lerp_common_ajax_productnumber_computechange'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-number-change[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductNumberController::class,
                        'action'     => 'computeChange'
                    ],
                ],
            ],
            'lerp_common_ajax_productnumber_autocomplete'                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-number-autocomplete[/:number]',
                    'constraints' => [
                        'number' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductNumberController::class,
                        'action'     => 'autocomplete'
                    ],
                ],
            ],
            'lerp_common_ajax_productnumber_autocompleteuuid'                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-number-autocomplete-uuid[/:number]',
                    'constraints' => [
                        'number' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductNumberController::class,
                        'action'     => 'autocompleteReturnUuid'
                    ],
                ],
            ],
            'lerp_common_ajax_productnumber_productbynumber'                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-by-number[/:number]',
                    'constraints' => [
                        'number' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductNumberController::class,
                        'action'     => 'productByNumber'
                    ],
                ],
            ],
            /*
             * AJAX - ProductVersion
             */
            'lerp_common_ajax_productversion_productversions'                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-products-for-product-no[/:uuid]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductVersionController::class,
                        'action'     => 'productsForNoUuid'
                    ],
                ],
            ],
            'lerp_common_ajax_productversion_copyproducttosingle'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-copy-to-single[/:uuid]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductVersionController::class,
                        'action'     => 'copyProductAsSingle'
                    ],
                ],
            ],
            'lerp_common_ajax_productversion_copyproducttoversion'            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-copy-to-version[/:uuid]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductVersionController::class,
                        'action'     => 'copyProductAsVersion'
                    ],
                ],
            ],
            'lerp_common_ajax_productversion_copyproducttorevision'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-copy-to-revision[/:uuid]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductVersionController::class,
                        'action'     => 'copyProductAsRevision'
                    ],
                ],
            ],
            /*
             * AJAX - supplier
             */
            'lerp_common_ajax_supplierproduct_productsuppliers'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-supplier-products[/:supplier_uuid]',
                    'constraints' => [
                        'supplier_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => SupplierProductAjaxController::class,
                        'action'     => 'supplierProducts'
                    ],
                ],
            ],
            /*
             * AJAX Maintenance
             */
            'lerp_common_ajax_maint_productmaintworkflow_allproductworkflows' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-ajax-maint-all-product-maint-workflows',
                    'defaults' => [
                        'controller' => Controller\Ajax\Maint\ProductMaintWorkflowController::class,
                        'action'     => 'allProductMaintWorkflows'
                    ],
                ],
            ],
            'lerp_common_ajax_maint_productmaintworkflow_updateorderpriority' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-ajax-maint-update-product-maint-workflow-order-priority',
                    'defaults' => [
                        'controller' => Controller\Ajax\Maint\ProductMaintWorkflowController::class,
                        'action'     => 'updateProductMaintWorkflowOrderPriority'
                    ],
                ],
            ],
            'lerp_common_ajax_maint_productmaint_updateorderpriority'         => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-ajax-maint-update-product-maint-order-priority',
                    'defaults' => [
                        'controller' => Controller\Ajax\Maint\ProductMaintController::class,
                        'action'     => 'updateProductMaintOrderPriority'
                    ],
                ],
            ],
            /*
             * AJAX workflow
             */
            'lerp_common_ajax_workflow_productworkflow_updateproductbriefing' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-product-ajax-workflow-update-product-briefing/:product_uuid',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductWorkflowAjaxController::class,
                        'action'     => 'updateProductBriefing'
                    ],
                ],
            ],
            /*
             * AJAX - Lists
             */
            'lerp_common_ajax_lists_productenums_productstructure'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-lists-productstructure',
                    'defaults' => [
                        'controller' => ProductEnumsController::class,
                        'action'     => 'productStructure'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_productenums_productorigin'               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-lists-productorigin',
                    'defaults' => [
                        'controller' => ProductEnumsController::class,
                        'action'     => 'productOrigin'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_productenums_producttype'                 => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-lists-producttype',
                    'defaults' => [
                        'controller' => ProductEnumsController::class,
                        'action'     => 'productType'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_producttables_productcclass'              => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-lists-productcclass',
                    'defaults' => [
                        'controller' => ProductTablesController::class,
                        'action'     => 'productCclass'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_producttables_productgroup'               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-lists-productgroup',
                    'defaults' => [
                        'controller' => ProductTablesController::class,
                        'action'     => 'productGroup'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_producttax_producttaxavailable'           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-tax-producttaxs',
                    'defaults' => [
                        'controller' => ProductTaxController::class,
                        'action'     => 'productTaxAvailable'
                    ],
                ],
            ],
            'lerp_common_ajax_lists_workflowtype_workflowtypes'               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-product-list-workflow-types',
                    'defaults' => [
                        'controller' => WorkflowTypeController::class,
                        'action'     => 'workflowTypes'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories' => [
            // REST
            ProductController::class                                    => ProductControllerFactory::class,
            ProductListRestController::class                            => ProductListRestControllerFactory::class,
            ProductListItemRestController::class                        => ProductListItemRestControllerFactory::class,
            ProductLocationController::class                            => ProductLocationControllerFactory::class,
            ProductTextController::class                                => ProductTextControllerFactory::class,
            WorkflowController::class                                   => WorkflowControllerFactory::class,
            ProductWorkflowRestController::class                        => ProductWorkflowRestControllerFactory::class,
            FileProductRestController::class                            => FileProductRestControllerFactory::class,
            // REST maintenance
            ProductMaintController::class                               => ProductMaintControllerFactory::class,
            ProductMaintDraftController::class                          => ProductMaintDraftControllerFactory::class,
            ProductMaintWorkflowController::class                       => ProductMaintWorkflowControllerFactory::class,
            // AJAX
            ProductNumberController::class                              => ProductNumberControllerFactory::class,
            ProductVersionController::class                             => ProductVersionControllerFactory::class,
            SupplierProductAjaxController::class                        => SupplierProductAjaxControllerFactory::class,
            // AJAX - Lists
            ProductEnumsController::class                               => ProductEnumsControllerFactory::class,
            ProductTablesController::class                              => ProductTablesControllerFactory::class,
            ProductTaxController::class                                 => ProductTaxControllerFactory::class,
            WorkflowTypeController::class                               => WorkflowTypeControllerFactory::class,
            // AJAX Maint
            Controller\Ajax\Maint\ProductMaintWorkflowController::class => Factory\Controller\Ajax\Maint\ProductMaintWorkflowControllerFactory::class,
            Controller\Ajax\Maint\ProductMaintController::class         => Factory\Controller\Ajax\Maint\ProductMaintControllerFactory::class,
            // AJAX workflow
            ProductWorkflowAjaxController::class                        => ProductWorkflowAjaxControllerFactory::class,
            // stream
            FileStreamController::class                                 => FileStreamControllerFactory::class,
        ]
    ],
    'service_manager' => [
        'invokables' => [
        ],
        'factories'  => [
            // table
            ProductTable::class                   => ProductTableFactory::class,
            ProductCclassTable::class             => ProductCclassTableFactory::class,
            ProductGroupTable::class              => ProductGroupTableFactory::class,
            ProductNoTable::class                 => ProductNoTableFactory::class,
            ProductListTable::class               => ProductListTableFactory::class,
            ProductTextTable::class               => ProductTextTableFactory::class,
            ProductLocationTable::class           => ProductLocationTableFactory::class,
            WorkflowTable::class                  => WorkflowTableFactory::class,
            WorkflowEquipmentGroupRelTable::class => WorkflowEquipmentGroupRelTableFactory::class,
            ProductWorkflowTable::class           => ProductWorkflowTableFactory::class,
            ProductMaintTable::class              => ProductMaintTableFactory::class,
            ProductMaintDraftTable::class         => ProductMaintDraftTableFactory::class,
            ProductMaintWorkflowTable::class      => ProductMaintWorkflowTableFactory::class,
            FileProductRelTable::class            => FileProductRelTableFactory::class,
            ProductSupplierTable::class           => ProductSupplierTableFactory::class,
            // service
            ProductGodService::class              => ProductGodServiceFactory::class,
            ProductService::class                 => ProductServiceFactory::class,
            ProductListService::class             => ProductListServiceFactory::class,
            ProductCalcService::class             => ProductCalcServiceFactory::class,
            EnumsService::class                   => EnumsServiceFactory::class,
            TablesService::class                  => TablesServiceFactory::class,
            ProductTextService::class             => ProductTextServiceFactory::class,
            WorkflowService::class                => WorkflowServiceFactory::class,
            ProductWorkflowService::class         => ProductWorkflowServiceFactory::class,
            ProductMaintService::class            => ProductMaintServiceFactory::class,
            FileProductRelService::class          => FileProductRelServiceFactory::class,
            // form
            ProductForm::class                    => ProductFormFactory::class,
            ProductTextForm::class                => ProductTextFormFactory::class,
            WorkflowForm::class                   => WorkflowFormFactory::class,
            ProductWorkflowForm::class            => ProductWorkflowFormFactory::class,
            ProductMaintForm::class               => ProductMaintFormFactory::class,
            ProductMaintDraftForm::class          => ProductMaintDraftFormFactory::class,
            ProductMaintWorkflowForm::class       => ProductMaintWorkflowFormFactory::class,
            FileProductForm::class                => FileProductFormFactory::class,
        ]
    ],
    'view_helpers'    => [
        'invokables' => [
        ]
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_product'    => [
        'module_brand'           => 'product',
        'product_taxp_available' => [19, 0],
    ]
];
