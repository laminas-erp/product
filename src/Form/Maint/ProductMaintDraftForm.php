<?php

namespace Lerp\Product\Form\Maint;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class ProductMaintDraftForm extends AbstractForm implements InputFilterProviderInterface
{

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'product_maint_draft_uuid']);
        }
        $this->add(['name' => 'product_maint_draft_label']);
        $this->add(['name' => 'product_maint_draft_desc']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['product_maint_draft_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class],
                    ['name' => StripTags::class]
                ], 'validators' => [
                    [
                        'name' => Uuid::class
                    ]
                ]
            ];
        }

        $filter['product_maint_draft_label'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['product_maint_draft_desc'] = [
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class],
                ['name' => StripTags::class]
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
