<?php

namespace Lerp\Product\Table\Maint;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Product\Entity\Maint\ProductMaintEntity;

class ProductMaintTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_maint';

    /**
     * @param string $productMaintUuid
     * @return array
     */
    public function getProductMaint(string $productMaintUuid): array
    {
        $select = $this->sql->select();
        $selectCount = new Select('product_maint_workflow');
        $selectCount->columns(['count_workflows' => new Expression('COUNT(product_maint_workflow_uuid)')]);
        try {
            $selectCount->where(['product_maint_uuid' => $productMaintUuid]);

            $select->columns([Select::SQL_STAR, 'count_workflows' => new Expression('?', [$selectCount])]);
            $select->where(['product_maint_uuid' => $productMaintUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductMaintsForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        $selectCount = new Select('product_maint_workflow');
        $selectCount->columns(['count_workflows' => new Expression('COUNT(product_maint_workflow_uuid)')]);
        try {
            $selectCount->where(['product_maint_uuid' => new Expression('product_maint.product_maint_uuid')]);

            $select->columns([Select::SQL_STAR, 'count_workflows' => new Expression('?', [$selectCount])]);
            $select->where(['product_uuid' => $productUuid]);
            $select->order('product_maint_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getProductMaintOrderPriorityMax(string $productUuid, bool $doMax = true): int
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            $select->order('product_maint_order_priority ' . ($doMax ? 'DESC' : 'ASC'));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0]['product_maint_order_priority'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getProductMaintsWithoutWorkflow(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectW = new Select('product_maint_workflow');
            $selectW->columns(['product_maint_uuid']);

            $select->where(['product_uuid' => $productUuid]);
            $select->where->notIn('product_maint_uuid', $selectW);
            $select->order('product_maint_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertProductMaint(array $storage): string
    {
        $insert = $this->sql->insert();
        if (empty($storage)) {
            return '';
        }
        $uuid = $this->uuid();
        $storage['product_maint_uuid'] = $uuid;
        $storage['product_maint_order_priority'] = $this->getProductMaintOrderPriorityMax($storage['product_uuid']) + 10;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $productUuid
     * @param int $orderPriority
     * @return array ['lessThan' => []; 'greaterThan' => []]
     */
    public function getOrderPriorityNeighbors(string $productUuid, int $orderPriority): array
    {
        $neighbors = [];
        $selectLessThan = $this->sql->select();
        $selectGreaterThan = $this->sql->select();
        try {
            $selectLessThan->columns(['product_maint_uuid', 'product_uuid', 'product_maint_order_priority']);
            $selectLessThan->where(['product_uuid' => $productUuid]);
            $selectLessThan->where->lessThan('product_maint_order_priority', $orderPriority);
            $selectLessThan->order('product_maint_order_priority DESC');
            /** @var HydratingResultSet $resultLessThan */
            $resultLessThan = $this->selectWith($selectLessThan);
            if (!$resultLessThan->valid() || $resultLessThan->count() > 0) {
                $neighbors['lessThan'] = $resultLessThan->toArray()[0];
            }

            $selectGreaterThan->columns(['product_maint_uuid', 'product_uuid', 'product_maint_order_priority']);
            $selectGreaterThan->where(['product_uuid' => $productUuid]);
            $selectGreaterThan->where->greaterThan('product_maint_order_priority', $orderPriority);
            $selectGreaterThan->order('product_maint_order_priority ASC');
            /** @var HydratingResultSet $result */
            $resultGreaterThan = $this->selectWith($selectGreaterThan);
            if ($resultGreaterThan->valid() && $resultGreaterThan->count() > 0) {
                $neighbors['greaterThan'] = $resultGreaterThan->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $neighbors;
    }

    /**
     * @param string $productMaintUuid
     * @param int $orderPriority
     * @return int
     */
    public function updateOrderPriority(string $productMaintUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_maint_order_priority' => $orderPriority
            ]);
            $update->where(['product_maint_uuid' => $productMaintUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param ProductMaintEntity $entity
     * @return int
     */
    public function updateProductMaint(ProductMaintEntity $entity): int
    {
        $update = $this->sql->update();
        $storage = $entity->getStorage();
        $uuid = $storage['product_maint_uuid'];
        unset($storage['product_maint_uuid']);
        try {
            $update->set($storage);
            $update->where(['product_maint_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteProductMaint(string $productMaintUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_maint_uuid' => $productMaintUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
