<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

/**
 * Class ProductCalcTable has no factory.
 * The class shows the existence of the table 'product_calc'.
 *
 * @package Lerp\Product\Table
 */
class ProductCalcTable extends AbstractLibTable
{
	/** @var string */
	protected $table = 'product_calc';

}
