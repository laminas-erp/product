<?php

namespace Lerp\Product\Controller\Ajax\Lists;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class ProductTaxController extends AbstractUserController
{
    protected array $productTaxAvailable = [];

    public function setProductTaxAvailable(array $productTaxAvailable): void
    {
        foreach ($productTaxAvailable as $productTax) {
            $this->productTaxAvailable[$productTax] = $productTax;
        }
    }

    /**
     * @return JsonModel
     */
    public function productTaxAvailableAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->productTaxAvailable);
        return $jsonModel;
    }
}
