<?php

namespace Lerp\Product\Factory\Service\Maint;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Service\Maint\ProductMaintService;
use Lerp\Product\Table\Maint\ProductMaintDraftTable;
use Lerp\Product\Table\Maint\ProductMaintTable;
use Lerp\Product\Table\Maint\ProductMaintWorkflowTable;

class ProductMaintServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductMaintService();
        $service->setLogger($container->get('logger'));
        $service->setProductMaintTable($container->get(ProductMaintTable::class));
        $service->setProductMaintDraftTable($container->get(ProductMaintDraftTable::class));
        $service->setProductMaintWorkflowTable($container->get(ProductMaintWorkflowTable::class));
        return $service;
    }
}
