<?php

namespace Lerp\Product\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ProductListTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'product_list';

    /**
     * @param string $productListUuid
     * @return array One product_list item.
     */
    public function getProductListItem(string $productListUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('product', 'product.product_uuid = product_list.product_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('product_no', 'product_no.product_no_uuid = product.product_no_uuid', ['product_no_no'], Select::JOIN_LEFT);
            $select->where(['product_list_uuid' => $productListUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected string $queryListAll = 'SELECT * FROM lerp_get_product_list_recursive(?) AS listall
    LEFT JOIN product_calc ON product_calc.product_uuid = listall.product_uuid -- can be null
    LEFT JOIN product ON product.product_uuid = listall.product_uuid
    LEFT JOIN product_no ON product_no.product_no_uuid = product.product_no_uuid
    LEFT JOIN quantityunit ON quantityunit.quantityunit_uuid = product.quantityunit_uuid';

    /**
     * Selects all from procedure lerp_get_product_list_recursive(product_uuid)
     * and JOIN product_calc, product, product_no and quantityunit.
     *
     * @param string $productUuid
     * @return array
     */
    public function getProductListRecursive(string $productUuid): array
    {
        $params = new ParameterContainer([$productUuid]);
        $stmt = $this->adapter->createStatement($this->queryListAll, $params);
        $result = $stmt->execute($params);
        $resultArr = [];
        if (!$result->valid() || $result->count() < 1) {
            return $resultArr;
        }
        do {
            $resultArr[] = $result->current();
            $result->next();
        } while ($result->valid());
        return $resultArr;
    }

    /**
     * @param string $productUuidParent
     * @param string $productUuid
     * @param float $quantity
     * @return string
     */
    public function insertProductListItem(string $productUuidParent, string $productUuid, float $quantity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'product_list_uuid' => $uuid,
                'product_uuid_parent' => $productUuidParent,
                'product_uuid' => $productUuid,
                'product_list_quantity' => $quantity,
                'product_list_order_priority' => $this->getMaxOrderPriority($productUuidParent) + 10,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getMaxOrderPriority(string $productUuidParent): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_priority' => new Expression('MAX(product_list_order_priority)')]);
            $select->where(['product_uuid_parent' => $productUuidParent]);
            $select->group('product_uuid_parent');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return intval($result->current()['max_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productListUuid
     * @return int
     */
    public function deleteProductListItem(string $productListUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['product_list_uuid' => $productListUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productUuidParent
     * @return array Array with product_uuid's
     */
    public function getProductListRecursiveParents(string $productUuidParent): array
    {
        $params = new ParameterContainer([$productUuidParent]);
        $stmt = $this->adapter->createStatement('SELECT * FROM lerp_get_product_list_recursive_parents(?)', $params);
        $result = $stmt->execute($params);
        $resultArr = [];
        if (!$result->valid() || $result->count() < 1) {
            return $resultArr;
        }
        do {
            $resultArr[] = $result->current()['product_uuid_parent'];
            $result->next();
        } while ($result->valid());
        return $resultArr;
    }

    public function getProductListItemsByListUuid(string $productListUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectUuidParent = $this->sql->select();
            $selectUuidParent->columns(['product_uuid_parent']);
            $selectUuidParent->where(['product_list_uuid' => $productListUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($selectUuidParent);
            if (!$result->valid() || $result->count() != 1) {
                return [];
            }

            $select->where(['product_uuid_parent' => $result->current()['product_uuid_parent']]);
            $select->order('product_list_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existProductListItem(string $productUuidParent, string $productUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid_parent' => $productUuidParent, 'product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $productListUuid
     * @param int $orderPriority
     * @return int
     */
    public function updateProductListItemOrderPriority(string $productListUuid, int $orderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_list_order_priority' => $orderPriority
            ]);
            $update->where(['product_list_uuid' => $productListUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $productListUuid
     * @param float $quantity
     * @return int
     */
    public function updateProductListItemQuantity(string $productListUuid, float $quantity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'product_list_quantity' => $quantity
            ]);
            $update->where(['product_list_uuid' => $productListUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
